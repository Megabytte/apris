using System;
using System.Text.RegularExpressions;

namespace AprisCompiler
{
    public class AssemblyOptimizer
    {
        private readonly Regex _uselessPushPop = new Regex(@"push (.*)\npop \1\n?");
        private readonly Regex _replacePushPopWithMove = new Regex(@"push (rax|rbx|rcx|rdx|rsi|rdi|rbp|rsp|r8|r9|r10|r11|r12|r13|r14|r15)\npop (rax|rbx|rcx|rdx|rsi|rdi|rbp|rsp|r8|r9|r10|r11|r12|r13|r14|r15)\n?");
        private readonly Regex _replaceMove0WithXor = new Regex(@"mov (rax|rbx|rcx|rdx|rsi|rdi|rbp|rsp|r8|r9|r10|r11|r12|r13|r14|r15), ?0\n?");
        private readonly Regex _removeRedundantRead = new Regex(@"mov (.*), ?(.*)\nmov \2, ?\1\n?");
        private readonly Regex _removeRedundantMove = new Regex(@"mov (.*), ?(.*)\nmov (.*), ?\1\npop \1");

        private delegate string RegexAction(string input, GroupCollection c);
        
        private static string PerformRegexOptimization(string input, Regex regexr, RegexAction regexAction)
        {
            for(;;)
            {
                var match = regexr.Match(input);

                if (!match.Success)
                {
                    break;
                }
                
                input = regexAction(input, match.Groups);
            }
            return input;
        }
        
        public string Optimize(string asmCode)
        {
            asmCode = _uselessPushPop.Replace(asmCode, "");
            
            asmCode = PerformRegexOptimization(asmCode, _replacePushPopWithMove, (input, groups) =>
            {
                string srcReg = groups[1].Value;
                string destReg = groups[2].Value;
                string rst = "mov " + destReg + ", " + srcReg + "\n";
                return input.Replace(groups[0].Value, rst);   
            });
            
            // Beware: it is not always safe to do this!
            // Xor will update the comparison flags thus possibly messing up code like this:
            //     cmp <reg A>, <val>
            //     xor <reg B>, <reg B> ; overwrites result of cmp!
            //     je <label>           ; possibly broken 
            /*
            asmCode = PerformRegexOptimization(asmCode, _replaceMove0WithXor, (input, groups) =>
            {
                string reg = groups[1].Value;
                string rst = "xor " + reg + ", " + reg + "\n";
                return input.Replace(groups[0].Value, rst);   
            });
            */

            asmCode = PerformRegexOptimization(asmCode, _removeRedundantRead, (input, groups) =>
            {
                string loc = groups[1].Value;
                string reg = groups[2].Value;
                string rst = "mov " + loc + ", " + reg + "\n";
                return input.Replace(groups[0].Value, rst);
            });
            
            asmCode = PerformRegexOptimization(asmCode, _removeRedundantMove, (input, groups) =>
            {
                string redundantReg = groups[1].Value;
                string value = groups[2].Value;
                string target = groups[3].Value;
                string rst = "mov " + target + ", " + value + "\npop " + redundantReg;
                return input.Replace(groups[0].Value, rst);
            });
            
            // TODO: A Regex that performs an operation like this:
            // push rax ; could be any register
            // <code that contains no pushes or pops or modifications to rax or sub registers like 'al'>
            // pop rax
            // the above can be replaced with just:
            // <code that contains no pushes or pops or modifications to rax or sub registers like 'al'>

            return asmCode;
        }
    }
}

/*
var a = 10;

if(a < 12 || (11 ^ 2) > 8)
{
    a |= 0xFF;
}
*/

/*
for(var i = 0; i < 10; i += 1)
{
    print(i);
    for(var j = 0; j < 10; j += 1)
    {
        print(j);
        if(j >= 4)
        {
            break break;
        }
    }
    print("Never Gets Here!");
}
print("End");

// prints: 0\n0\n1\n2\n3\n4\nEnd\n

fn add1(v1: u8, v2: u8) -> u8
{
    return v1 + v2;
}

fn add2(v1, v2)
{
    return v1 + v2;
}

fn add3(in v1: u8, in v2: u8) -> u8 {
    return v1 + v2;
}

fn do_set(out v: u8) -> u8 {
    v = 42;
    return 2;
}

fn change(do_change: bool, ref v: u8) -> void {
    if(do_change) { v = 47; }
}
*/
