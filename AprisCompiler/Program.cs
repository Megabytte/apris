﻿using System;
using System.Collections.Generic;
using System.IO;
using CommandLine;

/* Apris Language TODO:
 *    - Assembly Generation (x86 & ARM)
 *    - Tree Optimizations
 *    - Assembly Optimization (x86 & ARM)
 *    - ???
*/

namespace AprisCompiler
{
    public enum PlatformTargetType
    {
        Desktop64,
        ArmThumb32
    }
    
    public sealed class CompilerOptions
    {
        public bool VerbosityDebug;
        public bool VerbosityToken;
        public bool VerbosityParse;
        public bool VerbosityTreeOpt;
        public bool VerbosityAsm;
        public bool VerbosityAsmOpt;
            
        public bool WriteFilesDfa;
        public bool WriteFilesTable;
        public bool WriteFilesTree;
        public bool WriteFilesTreeOpt;
            
        public PlatformTargetType Target;
        public string SpecFilePath;
        public string InputFilePath;
        public string OutputFilePath;
    }
    
    internal static class Program
    {
        public static bool OnlyAssembleLinkAndRun;
        
        public static CompilerOptions ProgramOptions;
        
        private class Options
        {
            [Option('v', "verbose", Required = false, HelpText = "Enable extra logging. Options are: debug,token,parse,tree_opt,asm,asm_opt. Ex: -v debug token")]
            public IEnumerable<string> Verbosity { get; set; }
            
            [Option('w', Required = false, HelpText = "Enable extra debugging by emitting special files. Options are: dfa,table,tree,tree_opt. Ex: -v dfa table")]
            public IEnumerable<string> WriteFiles { get; set; }
            
            [Option('p', "platform", Required = false, Default = PlatformTargetType.Desktop64, HelpText = "Set compile target. Options are: Desktop64, ArmThumb32. Ex: -p ArmThumb32")]
            public PlatformTargetType Target { get; set; }
            
            [Option('s', "spec", Required = false, Default = "apris_spec.txt", HelpText = "The file path to the language spec for the compiler. Ex: -s spec.txt")]
            public string SpecFilePath { get; set; }
            
            [Option('i', "input", Required = true, HelpText = "The input file name to compile. Ex: -i program.apris")]
            public string InputFilePath { get; set; }
            
            [Option('o', "output", Required = false, HelpText = "The file name to output compiled code to. Ex: -o program.exe")]
            public string OutputFilePath { get; set; }
        }

        public static void Main(string[] args)
        {
            // change to 'true' to test with the Windows 10 Linux Subsystem
            ExeTools.UseLinuxSubsystem = false;
            
            // change to 'true' to have the compiler not overwrite the ASM yet still build and run it
            OnlyAssembleLinkAndRun = false;
            
            var argParser = new CommandLine.Parser(with =>
            {
                with.AutoHelp = true;
                with.AutoVersion = true;
                with.CaseSensitive = true;
                with.CaseInsensitiveEnumValues = true;
                with.IgnoreUnknownArguments = false;
                with.HelpWriter = Console.Out;
            });

            argParser.ParseArguments<Options>(args).WithParsed(opts =>
                {
                    ProgramOptions = new CompilerOptions
                    {
                        Target = opts.Target,
                        InputFilePath = opts.InputFilePath,
                        OutputFilePath = opts.OutputFilePath,
                        SpecFilePath = opts.SpecFilePath
                    };

                    foreach (string v in opts.Verbosity)
                    {
                        string l = v.ToLower();
                        switch (l)
                        {
                            case "debug":
                                ProgramOptions.VerbosityDebug = true;
                                break;
                            case "token":
                                ProgramOptions.VerbosityToken = true;
                                break;
                            case "parse":
                                ProgramOptions.VerbosityParse = true;
                                break;
                            case "tree_opt":
                                ProgramOptions.VerbosityTreeOpt = true;
                                break;
                            case "asm":
                                ProgramOptions.VerbosityAsm = true;
                                break;
                            case "asm_opt":
                                ProgramOptions.VerbosityAsmOpt = true;
                                break;
                            default: 
                                Console.Error.WriteLine("Unknown verbosity option: " + v);
                                Environment.Exit(0);
                                break;
                        }
                    }

                    foreach (string w in opts.WriteFiles)
                    {
                        string l = w.ToLower();
                        switch (l)
                        {
                            case "dfa":
                                ProgramOptions.WriteFilesDfa = true;
                                break;
                            case "table":
                                ProgramOptions.WriteFilesTable = true;
                                break;
                            case "tree":
                                ProgramOptions.WriteFilesTree = true;
                                break;
                            case "tree_opt":
                                ProgramOptions.WriteFilesTreeOpt = true;
                                break;
                            default:
                                Console.Error.WriteLine("Unknown write file option: " + w);
                                Environment.Exit(0);
                                break;
                        }
                    }
                })
                .WithNotParsed(errs =>
                {
                    
                    Console.Error.WriteLine("Commandline Arguments Error:");
                    foreach (var err in errs)
                    {
                        Console.Error.WriteLine("\tError Code: " + err.Tag);
                        Console.Error.WriteLine("\t\tStops Processing: " + err.StopsProcessing);
                        Console.Error.WriteLine();
                    }

                    Environment.Exit(1);
                });
            
            //SpecFileParser specFileParser = new SpecFileParser(ProgramOptions.SpecFilePath, true);
            // Console.WriteLine(specFileParser);

            Utility.LoadGlrParseTableFromFile("apris.glr", out var glrTable, out var terminals, out var nonTerminals);
            
            var tokenizer = new Tokenizer(new List<Terminal>(terminals));
            var tokenStream = tokenizer.Tokenize(ProgramOptions.InputFilePath, true);
            tokenStream.RemoveAll(token => token.Type == "WHITESPACE");
            if (ProgramOptions.VerbosityToken)
            {
                Console.WriteLine("Token Stream:");
                foreach (var token in tokenStream)
                {
                    Console.WriteLine("\t" + token);
                }
                Console.WriteLine();
            }

            if (tokenStream.Count == 0)
            {
                Console.Error.WriteLine("Cannot compile! Empty file!");
                return;
            }

            // dot -Tsvg lr0dfa.dot -o lr0dfa.svg
            var parser = new Parser(new List<NonTerminal>(nonTerminals)[0], glrTable, terminals, nonTerminals);
            //var parser = new Parser(specFileParser.StartSymbol, specFileParser.Tokens, specFileParser.Productions);

            ParseTreeNode tree;
            try
            {
                tree = parser.Parse(tokenStream);
                if (tree == null)
                {
                    throw new Exception("Parse Tree is Null.");
                }
            }
            catch (Exception e)
            {
                Console.Error.Write("Compiler Error: ");
                Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine("Full Exception: " + e);
                return;
            }

            // Tree Transformation Engine
            var treeOptimizer = new TreeOptimizer();
            AddTrimRules(treeOptimizer);
            AddBasicReductionRules(treeOptimizer);
            AddComplexReductionRules(treeOptimizer);
            AddComplexSymbolicReductionRules(treeOptimizer);

            Utility.SaveParseTreeToDot("parseTree.dot", tree);

            tree = treeOptimizer.Optimize(tree);
            Utility.SaveParseTreeToDot("parseTreeOptimized.dot", tree);

            var generator = new AssemblyGenerator(ProgramOptions);
            string asmCode = generator.Generate(tree);
            var asmOptimizer = new AssemblyOptimizer();
            string optimizedAsmCode = asmOptimizer.Optimize(asmCode);

            string inputFileName = Path.GetFileName(ProgramOptions.InputFilePath);
            string inputFileNameExt = Path.GetExtension(ProgramOptions.InputFilePath);

            if (inputFileName == null || inputFileNameExt == null)
            {
                throw new Exception("Should not be possible!");
            }

            string asmFileName = inputFileName.Replace(inputFileNameExt, ".asm");
            string objFileName = inputFileName.Replace(inputFileNameExt, ".obj");
            
            if (string.IsNullOrEmpty(ProgramOptions.OutputFilePath))
            {
                ProgramOptions.OutputFilePath = inputFileName.Replace(inputFileNameExt, ExeTools.OperatingSystem == ExeTools.Os.Win ? ".exe" : "");
            }

            if (ProgramOptions.VerbosityAsmOpt)
            {
                File.WriteAllText(asmFileName.Replace(".asm", "_unoptimized.asm"), asmCode);
            }

            if (!OnlyAssembleLinkAndRun)
            {
                File.WriteAllText(asmFileName, optimizedAsmCode);
            }

            ExeTools.Assemble(asmFileName, objFileName);
            ExeTools.Link(objFileName, ProgramOptions.OutputFilePath);

            ExeTools.ExeTestMode = true;
            ExeTools.Run("", (ExeTools.UseLinuxSubsystem ? "./" : "") + ProgramOptions.OutputFilePath);
            ExeTools.ExeTestMode = false;
            
            Console.WriteLine("Done.");
        }

        private static void AddTrimRules(TreeOptimizer treeOptimizer)
        {
            
        }
        
        private static void AddBasicReductionRules(TreeOptimizer treeOptimizer)
        {
            
        }
        
        private static void AddComplexReductionRules(TreeOptimizer treeOptimizer)
        {
            
        }
        
        private static void AddComplexSymbolicReductionRules(TreeOptimizer treeOptimizer)
        {
            
        }
    }
}
