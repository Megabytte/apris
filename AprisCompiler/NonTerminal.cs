using System;
using System.Collections.Generic;
using System.Text;

namespace AprisCompiler
{
    public class Production : IEquatable<Production>
    {
        public readonly string Lhs;
        public readonly List<string> Rhs;
        
        public Production(string lhs, List<string> rhs)
        {
            Lhs = lhs;
            Rhs = rhs;
        }

        public bool Equals(Production other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(Lhs, other.Lhs) && Equals(Rhs, other.Rhs);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Production) obj);
        }

        public override int GetHashCode()
        {
            unchecked // just ignores integer overflows
            {
                return ((Lhs != null ? Lhs.GetHashCode() : 0) * 397) ^ (Rhs != null ? Rhs.GetHashCode() : 0);
            }
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder(Lhs);
            builder.Append(" -> ");
            
            if (Rhs.Count == 0)
            {
                builder.Append("lambda");
            }
            else
            {
                for(int i = 0; i < Rhs.Count; i++)
                {
                    builder.Append(Rhs[i]);
                    if (i < Rhs.Count - 1)
                    {
                        builder.Append(" ");
                    }
                }
            }

            return builder.ToString();
        }
    }
    
    public class NonTerminal : IEquatable<NonTerminal>
    {
        public readonly string Lhs;
        public readonly List<Production> Rhs;

        public NonTerminal(string lhs, List<List<string>> rhs)
        {
            Lhs = lhs;
            Rhs = new List<Production>();
            foreach (List<string> list in rhs)
            {
                Rhs.Add(new Production(lhs, list));
            }
        }

        public bool Equals(NonTerminal other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(Lhs, other.Lhs) && Equals(Rhs, other.Rhs);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((NonTerminal) obj);
        }

        public override int GetHashCode()
        {
            unchecked // just ignores integer overflows
            {
                return ((Lhs != null ? Lhs.GetHashCode() : 0) * 397) ^ (Rhs != null ? Rhs.GetHashCode() : 0);
            }
        }

        public override string ToString()
        {
            string str = Lhs + " -> ";
            
            foreach (Production subProduction in Rhs)
            {
                str += subProduction + " | ";
            }
            
            return str.Substring(0, str.Length - 3);
        }
    }
}
