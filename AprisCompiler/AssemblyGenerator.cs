using System;
using System.Collections.Generic;
using System.Linq;

namespace AprisCompiler
{
    public class AssemblyGenerator
    {
        public AssemblyGenerator(CompilerOptions options)
        {
            _options = options;
        }

        private readonly CompilerOptions _options;
        private bool _emissionEnabled = true;
        private readonly List<string> _asmCode = new List<string>();
        private readonly List<Scope> _scopes = new List<Scope>();
        private readonly Scope _globalScope = new Scope { IsGlobalScope = true};

        private readonly Stack<VariableType> _typeStack = new Stack<VariableType>();

        private void Emit(string fmt, params object[] p)
        {
            if(!_emissionEnabled) { return; }
            
            //_asmCode.Add(string.Format(fmt, p));
            _asmCode.Add(fmt.Format(p));
        }

        private int _labelCounter;
        private string Label()
        {
            return "lbl" + _labelCounter++;
        }

        private void DoForeignFunctionCall(string foreignFunction)
        {
            Emit("mov rbx, rsp");
            Emit("and rsp, -16");
            if (ExeTools.OperatingSystem == ExeTools.Os.Win)
            {
                Emit("sub rsp,32");
            }
            Emit("call {0}", foreignFunction);
            if (ExeTools.OperatingSystem == ExeTools.Os.Win)
            {
                Emit("add rsp,32");
            }
            Emit("mov rsp, rbx");
        }

        private void PrologueCode()
        {
            Emit("push rbp");
            Emit("mov rbp, rsp");
        }

        private void EpilogueCode()
        {
            Emit("mov rsp, rbp");
            Emit("pop rbp");
            Emit("ret");
        }
        
        private void PushScope(Scope s=null)
        {
            _scopes.Add(s == null ? new Scope() : s);
        }

        private void PopScope()
        {
            _scopes.RemoveAt(_scopes.Count - 1);
        }

        private Scope GetCurrentScope()
        {
            return _scopes[_scopes.Count - 1];
        }
        
        private Tuple<int, Scope, Variable> GetVariableFromScope(string identifier)
        {
            for (int i = _scopes.Count - 1; i >= 0; i--)
            {
                if (_scopes[i].Variables.ContainsKey(identifier))
                {
                    return new Tuple<int, Scope, Variable>(i, _scopes[i], _scopes[i].Variables[identifier]);
                }
            }
            
            throw new Exception("Variable: \"{0}\" undeclared!".Format(identifier));
        }
        
        private Tuple<int, Scope, Function> GetFunctionFromScope(string identifier)
        {
            for (int i = _scopes.Count - 1; i >= 0; i--)
            {
                if (_scopes[i].Functions.ContainsKey(identifier))
                {
                    return new Tuple<int, Scope, Function>(i, _scopes[i], _scopes[i].Functions[identifier]);
                }
            }
            
            throw new Exception("Function: \"{0}\" undeclared!".Format(identifier));
        }

        private void AddVariableToScope(string identifier, Variable variable)
        {
            var scope = GetCurrentScope();
            
            if (scope.Variables.ContainsKey(identifier))
            {
                throw new Exception("Variable: \"" + identifier + "\" already declared!");
            }

            if (scope.Functions.ContainsKey(identifier))
            {
                throw new Exception("Identifier: \"" + identifier + "\" already declared as Function, identifier is ambiguous!");
            }
            
            scope.Variables.Add(identifier, variable);
        }
        
        private void AddFunctionToScope(string identifier, Function function)
        {
            var scope = GetCurrentScope();
            
            if (scope.Variables.ContainsKey(identifier))
            {
                throw new Exception("Identifier: \"" + identifier + "\" already declared as Variable, identifier is ambiguous!");
            }

            if (scope.Functions.ContainsKey(identifier))
            {
                throw new Exception("Function: \"" + identifier + "\" already declared!");
            }
            
            scope.Functions.Add(identifier, function);
        }
        
        private void ConvertTypes(VariableType from, VariableType to)
        {
            // TODO: This method is going to suck
            
            if (from == to)
            {
                return;
            }
            
            if (AsmGenUtil.IsIntegerType(from.BaseType) && to.BaseType == BasicType.Bool)
            {
                Emit("pop {0}", AsmGenUtil.GetArgRegister(0));
                Emit("call intToBool");
                Emit("push rax");
                return;
            }
            
            Console.WriteLine("\nWarning: " + from.TypeName + " -> " + to.TypeName + " is not implemented yet!\n");
        }
        
        public string Generate(ParseTreeNode tree)
        {
            Emit("default rel");
            Emit("");
            Emit("extern fopen");
            Emit("extern fclose");
            Emit("extern fflush");
            Emit("extern printf");
            Emit("extern fprintf");
            Emit("extern fscanf");
            Emit("extern scanf");
            Emit("extern abort");
            Emit("global main");
            Emit("");
            Emit("section .text");
            Emit("main:");
            
            PushScope(_globalScope);
            StartNodeCode(tree);
            PopScope();
            
            // Emit("pop rax");
            Emit("mov rax, 42 ; sentinel value for debugging");
            Emit("ret");

            OutputBuiltinFunctions();

            Emit("");
            Emit("section .data");

            OutputMagicConstants();
            
            Emit("");
            Emit("section .bss");

            OutputGlobalVariables();

            return AsmGenUtil.LineArrayToString(_asmCode);
        }
        
        private static readonly string[] BuiltinFunctions = { "print", "input", "open", "close", "write", "read", "flush", "abort" };
        private static bool IsBuiltinFunction(string identifier)
        {
            return BuiltinFunctions.Contains(identifier);
        }
        
        private void OutputBuiltinFunctions()
        {
            // This is not very code space efficient, but it works for now. 

            string arg0Reg = AsmGenUtil.GetArgRegister(0);
            string arg1Reg = AsmGenUtil.GetArgRegister(1);

            Emit("");
            Emit("intToBool:");
            PrologueCode();
            Emit("mov rax, 0");
            Emit("cmp {0}, 0", arg0Reg);
            Emit("setne al");
            EpilogueCode();

            Emit("");
            Emit("printIxx:");
            PrologueCode();
            Emit("mov {0}, {1}", arg1Reg, arg0Reg);
            Emit("mov {0}, iXXStr", arg0Reg);
            Emit("mov rax, 0");
            DoForeignFunctionCall("printf");
            Emit("mov {0}, 0", arg0Reg);
            DoForeignFunctionCall("fflush");
            EpilogueCode();
            
            Emit("");
            Emit("printUxx:");
            PrologueCode();
            Emit("mov {0}, {1}", arg1Reg, arg0Reg);
            Emit("mov {0}, uXXStr", arg0Reg);
            Emit("mov rax, 0");
            DoForeignFunctionCall("printf");
            Emit("mov {0}, 0", arg0Reg);
            DoForeignFunctionCall("fflush");
            EpilogueCode();
            
            Emit("");
            Emit("printI64:");
            PrologueCode();
            Emit("mov {0}, {1}", arg1Reg, arg0Reg);
            Emit("mov {0}, i64Str", arg0Reg);
            Emit("mov rax, 0");
            DoForeignFunctionCall("printf");
            Emit("mov {0}, 0", arg0Reg);
            DoForeignFunctionCall("fflush");
            EpilogueCode();
            
            Emit("");
            Emit("printU64:");
            PrologueCode();
            Emit("mov {0}, {1}", arg1Reg, arg0Reg);
            Emit("mov {0}, u64Str", arg0Reg);
            Emit("mov rax, 0");
            DoForeignFunctionCall("printf");
            Emit("mov {0}, 0", arg0Reg);
            DoForeignFunctionCall("fflush");
            EpilogueCode();
            
            Emit("");
            Emit("printFlt:");
            PrologueCode();
            Emit("mov {0}, {1}", arg1Reg, arg0Reg);
            Emit("mov {0}, fltStr", arg0Reg);
            Emit("movq xmm0, {0}", arg1Reg);
            Emit("mov rax, 1");
            DoForeignFunctionCall("printf");
            Emit("mov {0}, 0", arg0Reg);
            DoForeignFunctionCall("fflush");
            EpilogueCode();
            
            Emit("");
            Emit("printChr:");
            PrologueCode();
            Emit("mov {0}, {1}", arg1Reg, arg0Reg);
            Emit("mov {0}, chrStr", arg0Reg);
            Emit("mov rax, 0");
            DoForeignFunctionCall("printf");
            Emit("mov {0}, 0", arg0Reg);
            DoForeignFunctionCall("fflush");
            EpilogueCode();
            
            Emit("");
            Emit("printString:");
            PrologueCode();
            Emit("mov {0}, {1}", arg1Reg, arg0Reg);
            Emit("mov {0}, stringStr", arg0Reg);
            Emit("mov rax, 0");
            DoForeignFunctionCall("printf");
            Emit("mov {0}, 0", arg0Reg);
            DoForeignFunctionCall("fflush");
            EpilogueCode();
        }

        private void OutputMagicConstants()
        {
            Emit("uXXStr:");
            Emit("db `%u\\n`, 0");
            Emit("iXXStr:");
            Emit("db `%i\\n`, 0");
            Emit("i64Str:");
            Emit("db `%li\\n`, 0");
            Emit("u64Str:");
            Emit("db `%lu\\n`, 0");
            Emit("fltStr:");
            Emit("db `%f\\n`, 0");
            Emit("chrStr:");
            Emit("db `%c\\n`, 0");
            Emit("stringStr:");
            Emit("db `%s\\n`, 0");
        }

        private void OutputGlobalVariables()
        {
            foreach ((string identifier, Variable variable) in _globalScope.Variables)
            {
                Emit("{0}: ; {1}", variable.Identifier, identifier);
                Emit("resb {0}", variable.Type.Size);
            }
        }
        
        private void StartNodeCode(ParseTreeNode node)
        {
            // S -> statements | lambda
            // Note: lambda is an added construct here and does not exist in the grammar

            if (node.Children.Count == 0)
            {
                return;
            }
            
            StatementsNodeCode(node.Children[0]);
        }

        private void StatementsNodeCode(ParseTreeNode node)
        {
            // statements -> statement statements | statement
            
            if (node.Children.Count == 1)
            {
                StatementNodeCode(node.Children[0]);
            }
            else
            {
                StatementNodeCode(node.Children[0]);
                StatementsNodeCode(node.Children[1]);
            }
        }

        private void StatementNodeCode(ParseTreeNode node)
        {
            // statement -> variable_decl | conditional | for_loop | func_decl | struct_or_union_decl |
            //              class_decl | enum_decl | interface_decl | loop | while_loop | expr SEMI |
            //              return_stmt | break_or_continue_stmt SEMI | import_statement | tuple_unpack | block
            
            switch (node.Children[0].Symbol)
            {
                case "variable_decl": 
                    VariableDeclNodeCode(node.Children[0]);
                    break;
                case "conditional":
                    ConditionalNodeCode(node.Children[0]);
                    break;
                case "for_loop":
                    ForLoopNodeCode(node.Children[0]);
                    break;
                case "func_decl":
                    FuncDeclNodeCode(node.Children[0]);
                    break;
                case "struct_or_union_decl":
                    StructOrUnionDeclNodeCode(node.Children[0]);
                    break;
                case "class_decl":
                    ClassDeclNodeCode(node.Children[0]);
                    break;
                case "enum_decl":
                    EnumDeclNodeCode(node.Children[0]);
                    break;
                case "interface_decl":
                    InterfaceDeclNodeCode(node.Children[0]);
                    break;
                case "loop":
                    LoopNodeCode(node.Children[0]);
                    break;
                case "while_loop":
                    WhileLoopNodeCode(node.Children[0]);
                    break;
                case "expr":
                    ExprNodeCode(node.Children[0]);
                    break;
                case "return_stmt":
                    ReturnStmtNodeCode(node.Children[0]);
                    break;
                case "break_or_continue_stmt":
                    BreakOrContinueStmtNodeCode(node.Children[0]);
                    break;
                case "import_statement":
                    ImportStatementNodeCode(node.Children[0]);
                    break;
                case "tuple_unpack":
                    TupleUnpackNodeCode(node.Children[0]);
                    break;
                case "block":
                    BlockNodeCode(node.Children[0]);
                    break;
                default: throw new Exception("Unknown Symbol: " + node.Children[0].Symbol);
            }
        }

        private void BlockNodeCode(ParseTreeNode node)
        {
            // block -> LCURLY statements RCURLY | LCURLY RCURLY

            if (node.Children.Count != 3)
            {
                // block -> LCURLY RCURLY
                return;
            }
            
            // block -> LCURLY statements RCURLY
            PushScope();
            StatementsNodeCode(node.Children[1]);
            PopScope();
        }
        
        private VariableType TypeDeclNodeCode(ParseTreeNode node)
        {
            // type_decl -> TYPE | ID | ID generics_decl | TYPE generics_decl | func_type

            if (node.Children[0].Symbol == "func_type")
            {
                return FuncTypeNodeCode(node.Children[0]);
            }
            
            var type = new VariableType();
            
            bool isType = node.Children[0].Symbol == "TYPE";
            string contents = node.Children[0].Token;

            if (isType)
            {
                type.BaseType = AsmGenUtil.GetBasicTypeFromString(contents);
                type.TypeName = AsmGenUtil.GetBasicTypeAsString(type.BaseType);
            }
            else
            {
                throw new NotImplementedException("Custom Types are not supported yet!");
            }

            if(node.Children.Count > 1)
            {
                type.Generics = GenericsDeclNodeCode(node.Children[1]);
            }
            
            type.Size = AsmGenUtil.GetBasicTypeByteSize(type.BaseType);
            foreach (var variableType in type.Generics)
            {
                type.Size += variableType.Size;
            }

            return type;
        }
        
        private FunctionType FuncTypeNodeCode(ParseTreeNode node)
        {
            // func_type -> FN |
            //              FN LESS_THAN GREATER_THAN |
            //              FN LESS_THAN type_decl GREATER_THAN |
            //              FN LP RP |
            //              FN LP func_decl_args RP |
            //              FN LESS_THAN type_decl GREATER_THAN LP func_decl_args RP |
            //              FN LESS_THAN type_decl GREATER_THAN LP RP |
            //              FN LESS_THAN GREATER_THAN LP func_decl_args RP |
            //              FN LESS_THAN GREATER_THAN LP RP
            
            throw new NotImplementedException("Function Types are not implemented yet!");
        }
        
        private List<VariableType> GenericsDeclNodeCode(ParseTreeNode node)
        {
            // generics_decl -> LESS_THAN generics_decl_list GREATER_THAN
            var lst = new List<VariableType>(); 
            GenericsDeclListNodeCode(node.Children[1], ref lst);
            return lst;
        }

        private void GenericsDeclListNodeCode(ParseTreeNode node, ref List<VariableType> lst)
        {
            // generics_decl_list -> type_decl COMMA generics_decl_list | type_decl
            
            for(;;)
            {
                lst.Add(TypeDeclNodeCode(node.Children[0]));
                
                if (node.Children.Count > 1)
                {
                    // once done then,
                    node = node.Children[2];
                    continue;
                }

                break;
            }
        }

        private void GenericsCallNodeCode(ParseTreeNode node)
        {
            // generics_call -> LESS_THAN generics_call_list GREATER_THAN
            GenericsCallListNodeCode(node.Children[1]);
        }

        private void GenericsCallListNodeCode(ParseTreeNode node)
        {
            // generics_call_list ->
            //                       type_decl |
            //                       type_decl COMMA generics_call_list |
            //                       generics_call_literal |
            //                       generics_call_literal COMMA generics_call_list
            
            // TODO: Build up list of types
            
            for(;;)
            {
                if (node.Children[0].Symbol == "type_decl")
                {
                    // TODO: add to list of types as type
                    TypeDeclNodeCode(node.Children[0]);
                }
                else
                {
                    // TODO: add to list of types (as value?)
                    GenericsCallLiteralNodeCode(node.Children[0]);
                }

                var last = node.Children[node.Children.Count - 1];
                if (last.Symbol == "generics_call_list")
                {
                    // once done then,
                    node = last;
                    continue;
                }

                break;
            }
        }

        private void GenericsCallLiteralNodeCode(ParseTreeNode node)
        {
            // generics_call_literal ->
            //                          OCT_LIT |
            //                          HEX_LIT |
            //                          BIN_LIT |
            //                          FLT_LIT |
            //                          INT_LIT |
            //                          STR_LIT |
            //                          CHR_LIT |
            //                          BLN_LIT |
            //                          ary_lit |
            //                          tpl_lit |
            //                          obj
            
            
        }
        
        private void ImportStatementNodeCode(ParseTreeNode node)
        {
            // import_statement -> IMPORT ID SEMI

            string identifier = node.Children[1].Token;
            
            throw new NotImplementedException("Import statements are not implemented yet!");
        }
        
        private void ReturnStmtNodeCode(ParseTreeNode node)
        {
            // return_stmt -> RETURN expr SEMI | RETURN SEMI
            
            if (node.Children.Count == 3)
            {
                var type = ExprNodeCode(node.Children[1]);
                throw new NotImplementedException("Return statements with expressions are not implemented yet!");
                Emit("ret");
            }
            else
            {
                Emit("ret");
            }
        }
        
        private void BreakOrContinueStmtNodeCode(ParseTreeNode node)
        {
            // break_or_continue_stmt -> CONTINUE | break_stmt
            
            throw new NotImplementedException("Break & Continue statements are not implemented yet!");
            
            if (node.Children[0].Symbol == "break_stmt")
            {
                BreakStmtNodeCode(node.Children[0]);
            }
            else
            {
                Emit("; continue statement");
            }
        }
        
        private void BreakStmtNodeCode(ParseTreeNode node)
        {
            // break_stmt -> BREAK | BREAK break_or_continue_stmt

            if (node.Children.Count > 1)
            {
                BreakOrContinueStmtNodeCode(node.Children[1]);
            }
        }
        
        private void FuncDefDeclNodeCode(ParseTreeNode node)
        {
            // func_def_decl -> FN ID LP func_decl_args RP |
            //                  FN ID LP RP |
            //                  FN ID LP func_decl_args RP fn_rtn type_decl |
            //                  FN ID LP RP fn_rtn type_decl |
            //                  FN ID generics_decl LP func_decl_args RP |
            //                  FN ID generics_decl LP RP |
            //                  FN ID generics_decl LP func_decl_args RP fn_rtn type_decl |
            //                  FN ID generics_decl LP RP fn_rtn type_decl 
            
            switch (node.Children.Count)
            {
                case 4:
                {
                    // func_def_decl -> FN ID LP RP
                    
                }
                    return;
                case 5:
                {
                    switch (node.Children[3].Symbol)
                    {
                        case "RP":
                        {
                            // func_def_decl -> FN ID LP RP fn_rtn type_decl
                            TypeDeclNodeCode(node.Children[5]);
                        }
                            break;
                        case "func_decl_args":
                        {
                            // func_def_decl -> FN ID LP func_decl_args RP
                            FuncDeclArgsNodeCode(node.Children[3]);
                        }
                            break;
                        case "LP":
                        {
                            // func_def_decl -> FN ID generics_decl LP RP
                            GenericsDeclNodeCode(node.Children[2]);
                        }
                            break;
                        default:
                            throw new Exception("Unexpected Symbol: " + node.Children[3].Symbol);
                    }
                }
                    break;
                case 6:
                {
                    // func_def_decl -> FN ID generics_decl LP func_decl_args RP
                    GenericsDeclNodeCode(node.Children[2]);
                    FuncDeclArgsNodeCode(node.Children[4]);
                }
                    break;
                case 7:
                {
                    if (node.Children[2].Symbol == "LP")
                    {
                        // func_def_decl -> FN ID LP func_decl_args RP fn_rtn type_decl
                        FuncDeclArgsNodeCode(node.Children[3]);
                        TypeDeclNodeCode(node.Children[6]);
                    }
                    else
                    {
                        // func_def_decl -> FN ID generics_decl LP RP fn_rtn type_decl
                        GenericsDeclNodeCode(node.Children[2]);
                        TypeDeclNodeCode(node.Children[6]);   
                    }
                }
                    break;
                case 8:
                {
                    // func_def_decl -> FN ID generics_decl LP func_decl_args RP fn_rtn type_decl
                    GenericsDeclNodeCode(node.Children[2]);
                    FuncDeclArgsNodeCode(node.Children[4]);
                    TypeDeclNodeCode(node.Children[7]);
                }
                    break;
                default: throw new Exception("Unknown child count: " + node.Children.Count);
            }
        }
        
        /*private void FuncDefDeclNodeCodeBackup(ParseTreeNode node)
        {
            // func_def_decl -> FN ID LP func_decl_args RP |
            //                  FN ID LP RP |
            //                  FN ID LP func_decl_args RP fn_rtn type_decl |
            //                  FN ID LP RP fn_rtn type_decl |
            //                  FN ID generics_decl LP func_decl_args RP |
            //                  FN ID generics_decl LP RP |
            //                  FN ID generics_decl LP func_decl_args RP fn_rtn type_decl |
            //                  FN ID generics_decl LP RP fn_rtn type_decl 

            string identifier = node.Children[1].Token;

            if (node.Children[2].Symbol == "LP")
            {
                if (node.Children[3].Symbol == "RP")
                {
                    if (node.Children.Count > 4)
                    {
                        // func_def_decl -> FN ID LP RP fn_rtn type_decl
                        TypeDeclNodeCode(node.Children[5]);
                    }
                    else
                    {
                        // func_def_decl -> FN ID LP RP
                    }
                }
                else
                {
                    // func_def_decl -> FN ID LP func_decl_args RP | FN ID LP func_decl_args RP fn_rtn type_decl
                    FuncDeclArgsNodeCode(node.Children[3]);
                    if (node.Children.Count > 5)
                    {
                        // func_def_decl -> FN ID LP func_decl_args RP fn_rtn type_decl
                        TypeDeclNodeCode(node.Children[6]);
                    }
                }
            }
            else
            {
                GenericsDeclNodeCode(node.Children[2]);
                if (node.Children[4].Symbol == "RP")
                {
                    if (node.Children.Count > 5)
                    {
                        // func_def_decl -> FN ID generics_decl LP RP fn_rtn type_decl
                        TypeDeclNodeCode(node.Children[6]);
                    }
                    else
                    {
                        // func_def_decl -> FN ID generics_decl LP RP
                    }
                }
                else
                {
                    // func_def_decl -> FN ID generics_decl LP func_decl_args RP |
                    //                  FN ID generics_decl LP func_decl_args RP fn_rtn type_decl
                    FuncDeclArgsNodeCode(node.Children[4]);

                    if (node.Children.Count > 6)
                    {
                        // func_def_decl -> FN ID generics_decl LP func_decl_args RP fn_rtn type_decl
                        TypeDeclNodeCode(node.Children[7]);
                    }
                }
            }
        }*/
        
        private void FuncProtoDeclNodeCode(ParseTreeNode node)
        {
            // func_proto_decl -> func_def_decl SEMI
            FuncDefDeclNodeCode(node.Children[0]);
        }
        
        private void FuncDeclNodeCode(ParseTreeNode node)
        {
            // func_decl -> func_def_decl block
            FuncDefDeclNodeCode(node.Children[0]);
            
            PrologueCode();
            BlockNodeCode(node.Children[1]);
            EpilogueCode();
        }

        private static string OriNodeCode(ParseTreeNode node)
        {
            // ori -> IN | OUT | REF
            return node.Children[0].Symbol;
        }

        private void FuncDeclArgsNodeCode(ParseTreeNode node)
        {
            // func_decl_args -> ori ID |
            //                   ori ID COLON type_decl |
            //                   ori ID COLON type_decl COMMA func_decl_args |
            //                   ori ID COMMA func_decl_args |
            //                   IN ID ASSIGN expr |
            //                   IN ID COLON type_decl ASSIGN expr |
            //                   IN ID COLON type_decl ASSIGN expr COMMA func_decl_args_default_forced |
            //                   IN ID COMMA func_decl_args_default_forced ASSIGN expr |
            //                   ID |
            //                   ID COLON type_decl |
            //                   ID COLON type_decl COMMA func_decl_args |
            //                   ID COMMA func_decl_args |
            //                   ID ASSIGN expr |
            //                   ID COLON type_decl ASSIGN expr |
            //                   ID COLON type_decl ASSIGN expr COMMA func_decl_args_default_forced |
            //                   ID COMMA func_decl_args_default_forced ASSIGN expr |
            //                   ID COLON type_decl LBRACE RBRACE DOT DOT DOT |
            //                   ID DOT DOT DOT
            
            switch (node.Children[0].Symbol)
            {
                case "ori":
                {
                    string type = OriNodeCode(node.Children[0]);
                    string identifier = node.Children[1].Token;

                    if (node.Children.Count > 2)
                    {
                        if (node.Children[2].Symbol == "COLON")
                        {
                            TypeDeclNodeCode(node.Children[3]);
                            
                            if (node.Children.Count > 4)
                            {
                                FuncDeclArgsNodeCode(node.Children[5]);
                            }
                        }
                        else
                        {
                            FuncDeclArgsNodeCode(node.Children[3]);
                        }
                    }
                }
                    break;
                case "IN":
                {
                    string identifier = node.Children[1].Token;

                    switch (node.Children[2].Symbol)
                    {
                        case "ASSIGN":
                        {
                            ExprNodeCode(node.Children[3]);
                        }
                            break;
                        case "COLON":
                        {
                            TypeDeclNodeCode(node.Children[3]);
                            ExprNodeCode(node.Children[5]);
                            if (node.Children.Count > 6)
                            {
                                FuncDeclArgsDefaultForcedNodeCode(node.Children[7]);
                            }
                        }
                            break;
                        case "COMMA":
                        {
                            FuncDeclArgsDefaultForcedNodeCode(node.Children[3]);
                            ExprNodeCode(node.Children[5]);
                        }
                            break;
                        default: throw new Exception("Unexpected Symbol: " + node.Children[2].Symbol);
                    }
                }
                    break;
                case "ID":
                {
                    string identifier = node.Children[0].Token;

                    if (node.Children.Count == 1)
                    {
                        // func_decl_args -> ID

                        return;
                    }

                    switch (node.Children[1].Symbol)
                    {
                        case "ASSIGN":
                        {
                            ExprNodeCode(node.Children[2]);
                        }
                            break;
                        case "DOT":
                        {
                            // func_decl_args -> ID DOT DOT DOT
                        }
                            break;
                        case "COMMA":
                        {
                            if (node.Children[2].Symbol == "func_decl_args")
                            {
                                FuncDeclArgsNodeCode(node.Children[2]);
                            }
                            else
                            {
                                FuncDeclArgsDefaultForcedNodeCode(node.Children[2]);
                                ExprNodeCode(node.Children[4]);
                            }
                        }
                            break;
                        case "COLON":
                        {
                            TypeDeclNodeCode(node.Children[2]);

                            if (node.Children.Count > 3)
                            {
                                switch (node.Children[3].Symbol)
                                {
                                    case "ASSIGN":
                                    {
                                        // ID COLON type_decl ASSIGN expr
                                        ExprNodeCode(node.Children[4]);
                                        if (node.Children.Count > 5)
                                        {
                                            // ID COLON type_decl ASSIGN expr COMMA func_decl_args_default_forced
                                            FuncDeclArgsDefaultForcedNodeCode(node.Children[6]);
                                        }
                                    }
                                        break;
                                    case "COMMA":
                                    {
                                        // ID COLON type_decl COMMA func_decl_args
                                        FuncDeclArgsNodeCode(node.Children[4]);
                                    }
                                        break;
                                    case "LBRACE":
                                    {
                                        // ID COLON type_decl LBRACE RBRACE DOT DOT DOT
                                    }
                                        break;
                                    default: throw new Exception("Unexpected Symbol: " + node.Children[3].Symbol);
                                }
                            }
                        }
                            break;
                        default: throw new Exception("Unexpected Symbol: " + node.Children[1].Symbol);
                    }
                }
                    break;
                default: throw new Exception("Unexpected Symbol: " + node.Children[0].Symbol);
            }
        }

        private void FuncDeclArgsDefaultForcedNodeCode(ParseTreeNode node)
        {
            // func_decl_args_default_forced -> IN ID COLON type_decl ASSIGN expr COMMA func_decl_args_default_forced |
            //                                  IN ID COLON type_decl ASSIGN expr |
            //                                  IN ID COMMA func_decl_args_default_forced ASSIGN expr |
            //                                  IN ID ASSIGN expr |
            //                                  ID COLON type_decl ASSIGN expr |
            //                                  ID COLON type_decl ASSIGN expr COMMA func_decl_args_default_forced |
            //                                  ID COMMA func_decl_args_default_forced ASSIGN expr |
            //                                  ID ASSIGN expr
            
            int baseIndex = node.Children[0].Symbol == "IN" ? 1 : 0;
            string identifier = node.Children[baseIndex].Token;

            switch (node.Children[baseIndex + 1].Symbol)
            {
                case "COLON":
                {
                    TypeDeclNodeCode(node.Children[baseIndex + 2]);
                    ExprNodeCode(node.Children[baseIndex + 4]);
                    if (node.Children.Count > baseIndex + 6)
                    {
                        FuncDeclArgsDefaultForcedNodeCode(node.Children[baseIndex + 7]);
                    }
                }
                    break;
                case "COMMA":
                {
                    FuncDeclArgsDefaultForcedNodeCode(node.Children[baseIndex + 2]);
                    ExprNodeCode(node.Children[baseIndex + 4]);
                }
                    break;
                case "ASSIGN":
                {
                    ExprNodeCode(node.Children[baseIndex + 2]);
                }
                    break;
                default: throw new Exception("Unexpected Symbol: " + node.Children[baseIndex + 1].Symbol);
            }
        }

        private static string LetOrVarNodeCode(ParseTreeNode node)
        {
            // let_or_var -> LET | VAR
            return node.Children[0].Symbol;
        }
        
        private void VariableDeclNodeCode(ParseTreeNode node)
        {
            // variable_decl -> VAR ID COLON type_decl SEMI |
            //                  let_or_var ID COLON type_decl ASSIGN expr SEMI |
            //                  let_or_var ID COLON type_decl ASSIGN memory_decl SEMI |
            //                  let_or_var ID ASSIGN expr SEMI

            string letOrVar = node.Children[0].Symbol == "let_or_var" ? LetOrVarNodeCode(node.Children[0]) : node.Children[0].Symbol;
            string identifier = node.Children[1].Token;
            
            VariableType type;
            VariableType initializerType = null;

            if (node.Children[2].Symbol == "COLON")
            {
                type = TypeDeclNodeCode(node.Children[3]);
            }
            else
            {
                type = initializerType = ExprNodeCode(node.Children[3]);
            }
            
            if (node.Children.Count > 5)
            {
                if (node.Children[5].Symbol == "expr")
                {
                    initializerType = ExprNodeCode(node.Children[5]);
                }
                else
                {
                    type.MemoryInfo = MemoryDeclNodeCode(node.Children[5]);
                }
            }

            var v = new Variable {
                Identifier = identifier + "_" + Label(),
                IsConstant = letOrVar == "let",
                Type = type
            };
            AddVariableToScope(identifier, v);
            
            if (initializerType == null)
            {
                return;
            }
            
            var coercedExprType = AsmGenUtil.CoerceType(initializerType, type);
            ConvertTypes(initializerType, coercedExprType);
                
            Emit("pop rax");
            Emit("mov [{0}], rax", v.Identifier);
        }
        
        private MemoryInfo MemoryDeclNodeCode(ParseTreeNode node)
        {
            // memory_decl -> VOLATILE MEMORY LBRACE mem_lit RBRACE | MEMORY LBRACE mem_lit RBRACE

            int baseIndex = node.Children[0].Symbol == "VOLATILE" ? 1 : 0;
            ulong val = MemLitNodeCode(node.Children[baseIndex + 2]);

            return new MemoryInfo {
                UseMemory = true,
                IsVolatile = baseIndex == 1,
                Location = val
            };
        }
        
        private static ulong MemLitNodeCode(ParseTreeNode node)
        {
            // mem_lit -> HEX_LIT | BIN_LIT | INT_LIT | OCT_LIT

            string symbol = node.Children[0].Symbol;
            string literal = node.Children[0].Token;
            switch (symbol)
            {
                case "HEX_LIT": return (ulong) LiteralToInteger(literal, 16, out _);
                case "BIN_LIT": return (ulong) LiteralToInteger(literal, 2, out _);
                case "INT_LIT": return (ulong) LiteralToInteger(literal, 10, out _);
                case "OCT_LIT": return (ulong) LiteralToInteger(literal, 8, out _);
                default: throw new Exception("Unknown Symbol: " + symbol);
            }
        }

        private void TupleUnpackNodeCode(ParseTreeNode node)
        {
            // tuple_unpack -> LP var_decl_list RP ASSIGN expr SEMI
            
            VarDeclListNodeCode(node.Children[1]);
            ExprNodeCode(node.Children[4]);
        }

        private void VarDeclListNodeCode(ParseTreeNode node)
        {
            // var_decl_list -> let_or_var ID COLON type_decl COMMA var_decl_list |
            //                  let_or_var ID COMMA var_decl_list |
            //                  let_or_var ID COLON type_decl |
            //                  let_or_var ID

            string letOrVar = node.Children[0].Children[0].Symbol;
            string identifier = node.Children[1].Token;

            switch (node.Children.Count)
            {
                case 2:
                {
                    // var_decl_list -> let_or_var ID
                }
                    break;
                case 4:
                {
                    if (node.Children[2].Symbol == "COLON")
                    {
                        // var_decl_list -> let_or_var ID COLON type_decl
                        TypeDeclNodeCode(node.Children[3]);
                    }
                    else
                    {
                        // var_decl_list -> let_or_var ID COMMA var_decl_list
                        VarDeclListNodeCode(node.Children[3]);
                    }
                }
                    break;
                case 6:
                {
                    // var_decl_list -> let_or_var ID COLON type_decl COMMA var_decl_list |
                    TypeDeclNodeCode(node.Children[3]);
                    
                    VarDeclListNodeCode(node.Children[5]);
                }
                    break;
                default: throw new Exception("Unknown child count: " + node.Children.Count);
            }
        }

        private void ConditionalNodeCode(ParseTreeNode node)
        {
            // conditional -> IF LP expr RP block | IF LP expr RP block conditionalPrime
            
            var exprType = ExprNodeCode(node.Children[2]);
            var coersedExprType = AsmGenUtil.CoerceType(exprType, AsmGenUtil.BoolType);
            ConvertTypes(exprType, coersedExprType);

            Emit("pop rax");
            Emit("cmp rax, 0");
            
            string endLabel = Label();
            if (node.Children.Count == 5)
            {
                Emit("je {0}", endLabel);
                BlockNodeCode(node.Children[4]);
            }
            else
            {
                string elseLabel = Label();
                Emit("je {0}", elseLabel);
                BlockNodeCode(node.Children[4]);
                Emit("jmp {0}", endLabel);
                Emit("{0}:", elseLabel);
                ConditionalPrimeNodeCode(node.Children[5], endLabel);
            }
            Emit("{0}:", endLabel);
        }

        private void ConditionalPrimeNodeCode(ParseTreeNode node, string endLabel)
        {
            // conditionalPrime -> ELSE IF LP expr RP block conditionalPrime | ELSE IF LP expr RP block | ELSE block
            
            if (node.Children.Count == 2)
            {
                BlockNodeCode(node.Children[1]);
                return;
            }

            var exprType = ExprNodeCode(node.Children[3]);
            var coersedExprType = AsmGenUtil.CoerceType(exprType, AsmGenUtil.BoolType);
            ConvertTypes(exprType, coersedExprType);
            
            Emit("pop rax");
            Emit("cmp rax, 0");
            
            if (node.Children.Count <= 6)
            {
                Emit("je {0}", endLabel);
                BlockNodeCode(node.Children[5]);
            }
            else
            {
                string elseLabel = Label();
                Emit("je {0}", elseLabel);
                BlockNodeCode(node.Children[5]);
                Emit("jmp {0}", endLabel);
                Emit("{0}:", elseLabel);
                ConditionalPrimeNodeCode(node.Children[6], endLabel);
            }
        }

        private void LoopNodeCode(ParseTreeNode node)
        {
            // loop -> LOOP block

            // TODO: allow break/continue functionality here
            string startLabel = Label();
            string endLabel = Label();
            Emit("{0}:", startLabel);
            BlockNodeCode(node.Children[1]);
            Emit("jmp {0}", startLabel);
            Emit("{0}:", endLabel);
        }
        
        private void WhileLoopNodeCode(ParseTreeNode node)
        {
            // while_loop -> WHILE LP expr RP block
            
            string startLabel = Label();
            string endLabel = Label();
            
            Emit("{0}:", startLabel);
            var exprType = ExprNodeCode(node.Children[2]);
            var coersedExprType = AsmGenUtil.CoerceType(exprType, AsmGenUtil.BoolType);
            ConvertTypes(exprType, coersedExprType);
            Emit("pop rax");
            Emit("cmp rax, 0");
            Emit("je {0}", endLabel);
            BlockNodeCode(node.Children[4]);
            Emit("jmp {0}", startLabel);
            Emit("{0}:", endLabel);
        }
        
        private void ForLoopNodeCode(ParseTreeNode node)
        {
            // for_loop -> FOR LP LP var_decl_list RP IN expr RP block |
            //             FOR LP variable_decl expr SEMI expr RP block |
            //             FOR LP ID IN expr RP block
            
            switch (node.Children.Count)
            {
                case 7:
                {
                    // for_loop -> FOR LP ID IN expr RP block
                    string identifier = node.Children[2].Token;
                    ExprNodeCode(node.Children[4]);
                    BlockNodeCode(node.Children[6]);
                    
                    throw new NotImplementedException("For-each loop not implemented yet!");
                }
                    break;
                case 8:
                {
                    // for_loop -> FOR LP variable_decl expr SEMI expr RP block
                    
                    // TODO: fix the fact that this variable ends up in the scope above the for loop
                    VariableDeclNodeCode(node.Children[2]);
                    
                    string startLabel = Label();
                    string endLabel = Label();

                    Emit("{0}:", startLabel);
                    
                    var exprType = ExprNodeCode(node.Children[3]);
                    var coercedExprType = AsmGenUtil.CoerceType(exprType, AsmGenUtil.BoolType);
                    ConvertTypes(exprType, coercedExprType);
                    
                    Emit("pop rax");
                    Emit("cmp rax, 0");
                    Emit("je {0}", endLabel);
                    BlockNodeCode(node.Children[7]);
                    
                    ExprNodeCode(node.Children[5]);
                    
                    Emit("jmp {0}", startLabel);
                    Emit("{0}:", endLabel);
                }
                    break;
                case 9:
                {
                    // for_loop -> FOR LP LP var_decl_list RP IN expr RP block
                    VarDeclListNodeCode(node.Children[3]);
                    ExprNodeCode(node.Children[6]);
                    BlockNodeCode(node.Children[8]);
                    
                    throw new NotImplementedException("For-each (with unpack) loop not implemented yet!");
                }
                    break;
                default: throw new Exception("Unknown child count: " + node.Children.Count);
            }
        }
        
        private void FuncCallNodeCode(ParseTreeNode node, ref Function functionSignature)
        {
            // func_call -> LP func_call_args RP | LP RP
            
            if (node.Children.Count != 3)
            {
                return;
            }

            functionSignature.Type.Arguments = FuncCallArgsNodeCode(node.Children[1]);
        }

        private List<Tuple<string, string, VariableType>> FuncCallArgsNodeCode(ParseTreeNode node)
        {
            // func_call_args ->
            //                   expr |
            //                   expr COMMA func_call_args |
            //                   ori expr |
            //                   ori expr COMMA func_call_args |
            //                   ID COLON expr |
            //                   ID COLON expr COMMA func_call_args |
            //                   ID COLON ori expr |
            //                   ID COLON ori expr COMMA func_call_args

            var variables = new List<Tuple<string, string, VariableType>>();
            
            for(;;)
            {
                int baseIndex = node.Children[0].Symbol == "ID" ? 2 : 0;

                VariableType type;
                string passType;
                string namedParameter = baseIndex == 2 ? node.Children[0].Token : "";

                if (node.Children[baseIndex].Symbol == "expr")
                {
                    passType = "IN";
                    type = ExprNodeCode(node.Children[baseIndex]);
                }
                else
                {
                    passType = OriNodeCode(node.Children[baseIndex]);
                    type = ExprNodeCode(node.Children[baseIndex + 1]);
                }
                
                variables.Add(new Tuple<string, string, VariableType>(passType, namedParameter, type));

                var last = node.Children[node.Children.Count - 1];
                if (last.Symbol == "func_call_args")
                {
                    // once done then,
                    node = last;
                    continue;
                }

                break;
            }

            return variables;
        }

        private VariableType ExprNodeCode(ParseTreeNode node)
        {
            // expr -> obj assignment_type expr | struct_initializer | expr1
            
            switch (node.Children[0].Symbol)
            {
                case "obj":
                {
                    var objType = ObjNodeCode(node.Children[0], true);
                    string assignmentType = AssignmentTypeNodeCode(node.Children[1]);
                    var exprType = ExprNodeCode(node.Children[2]);

                    // TODO: Implement Assignment
                    // use operator=(), and such, on classes
                    
                    var coercedExprType = AsmGenUtil.CoerceType(exprType, objType);
                    ConvertTypes(exprType, coercedExprType);

                    if (assignmentType == "ASSIGN")
                    {
                        Emit("pop rcx"); // rhs
                        Emit("pop rax"); // lhs
                        Emit("mov [rax], rcx");
                    }
                    else
                    {
                        throw new NotImplementedException("Assignment Type '" + assignmentType + "' is not implemented yet!");
                    }
                    
                    return objType;
                }
                case "struct_initializer": return StructInitializerNodeCode(node.Children[0]);
                case "expr1": return Expr1NodeCode(node.Children[0]);
                default: throw new Exception("Unknown Symbol: " + node.Children[0].Symbol);
            }
        }

        private VariableType Expr1NodeCode(ParseTreeNode node)
        {
            // expr1 -> expr2 QUESTION expr COLON expr | expr2

            if (node.Children.Count == 1)
            {
                return Expr2NodeCode(node.Children[0]);    
            }

            string end = Label();
            string ifFalse = Label();
            
            var expr2Type = Expr2NodeCode(node.Children[0]);
            var coercedExpr2Type = AsmGenUtil.CoerceType(expr2Type, AsmGenUtil.BoolType);
            ConvertTypes(expr2Type, coercedExpr2Type);
            // TODO: Handle 'coercedExpr2Type'?

            Emit("pop rax");
            Emit("cmp rax, 0");
            Emit("je {0}", ifFalse);
            var exprType1 = ExprNodeCode(node.Children[2]);
            Emit("jmp {0}", end);
            Emit("{0}:", ifFalse);
            var exprType2 = ExprNodeCode(node.Children[4]);
            Emit("{0}:", end);

            var type = AsmGenUtil.CanCoerceTypes(exprType1, exprType2) ? 
                AsmGenUtil.CoerceType(exprType1, exprType2) : 
                AsmGenUtil.CoerceType(exprType2, exprType1);
            
            // TODO: Handle types?
            
            return type;
        }
        
        private VariableType Expr2NodeCode(ParseTreeNode node)
        {
            // expr2 -> expr2 logical_or expr3 | expr2 OR expr3 | expr3

            if (node.Children.Count == 1)
            {
                return Expr3NodeCode(node.Children[0]);
            }
            
            var expr2Type = Expr2NodeCode(node.Children[0]);
            var coercedExpr2Type = AsmGenUtil.CoerceType(expr2Type, AsmGenUtil.BoolType);
            ConvertTypes(expr2Type, coercedExpr2Type);
            
            var expr3Type = Expr3NodeCode(node.Children[2]);
            var coercedExpr3Type = AsmGenUtil.CoerceType(expr3Type, AsmGenUtil.BoolType);
            ConvertTypes(expr3Type, coercedExpr3Type);
            
            // TODO: Handle types
            // use operator||() on classes
            
            Emit("pop rcx");
            Emit("pop rax");
            Emit("or rax, rcx"); // results in number, while Bool is desired
            Emit("push rax");
            
            var coercedIntType = AsmGenUtil.CoerceType(AsmGenUtil.U64Type, AsmGenUtil.BoolType);
            ConvertTypes(AsmGenUtil.U64Type, coercedIntType);
            
            return coercedIntType;
        }
        
        private VariableType Expr3NodeCode(ParseTreeNode node)
        {
            // expr3 -> expr3 logical_and expr4 | expr3 AND expr4 | expr4
            
            if (node.Children.Count == 1)
            {
                return Expr4NodeCode(node.Children[0]);
            }
            
            var expr3Type = Expr3NodeCode(node.Children[0]);
            var coercedExpr3Type = AsmGenUtil.CoerceType(expr3Type, AsmGenUtil.BoolType);
            ConvertTypes(expr3Type, coercedExpr3Type);
            
            var expr4Type = Expr4NodeCode(node.Children[2]);
            var coercedExpr4Type = AsmGenUtil.CoerceType(expr4Type, AsmGenUtil.BoolType);
            ConvertTypes(expr4Type, coercedExpr4Type);
            
            // TODO: Handle types
            // use operator&&() on classes
            
            Emit("pop rcx");
            Emit("pop rax");
            Emit("and rax, rcx"); // results in number, while Bool is desired
            Emit("push rax");
            
            var coercedIntType = AsmGenUtil.CoerceType(AsmGenUtil.U64Type, AsmGenUtil.BoolType);
            ConvertTypes(AsmGenUtil.U64Type, coercedIntType);
            
            return coercedIntType;
        }
        
        private VariableType Expr4NodeCode(ParseTreeNode node)
        {
            // expr4 -> expr4 BITWISE_OR expr5 | expr5
            
            if (node.Children.Count == 1)
            {
                return Expr5NodeCode(node.Children[0]);
            }

            var expr4Type = Expr4NodeCode(node.Children[0]);
            var expr5Type = Expr5NodeCode(node.Children[2]);

            var type = AsmGenUtil.CanCoerceTypes(expr4Type, expr5Type) ? 
                AsmGenUtil.CoerceType(expr4Type, expr5Type) : 
                AsmGenUtil.CoerceType(expr5Type, expr4Type);
            
            ConvertTypes(expr4Type, type);
            ConvertTypes(expr5Type, type);
            
            Emit("pop rcx");
            Emit("pop rax");
            Emit("or rax, rcx");
            Emit("push rax");
            
            return type;
        }
        
        private VariableType Expr5NodeCode(ParseTreeNode node)
        {
            // expr5 -> expr5 BITWISE_XOR expr6 | expr6
            
            if (node.Children.Count == 1)
            {
                return Expr6NodeCode(node.Children[0]);
            }

            var expr5Type = Expr5NodeCode(node.Children[0]);
            var expr6Type = Expr6NodeCode(node.Children[2]);
            
            var type = AsmGenUtil.CanCoerceTypes(expr5Type, expr6Type) ? 
                AsmGenUtil.CoerceType(expr5Type, expr6Type) : 
                AsmGenUtil.CoerceType(expr6Type, expr5Type);
            
            ConvertTypes(expr5Type, type);
            ConvertTypes(expr6Type, type);
            
            Emit("pop rcx");
            Emit("pop rax");
            Emit("xor rax, rcx");
            Emit("push rax");
            
            return type;
        }
        
        private VariableType Expr6NodeCode(ParseTreeNode node)
        {
            // expr6 -> expr6 BITWISE_AND expr7 | expr7
            
            if (node.Children.Count == 1)
            {
                return Expr7NodeCode(node.Children[0]);
            }

            var expr6Type = Expr6NodeCode(node.Children[0]);
            var expr7Type = Expr7NodeCode(node.Children[2]);
            
            var type = AsmGenUtil.CanCoerceTypes(expr6Type, expr7Type) ? 
                AsmGenUtil.CoerceType(expr6Type, expr7Type) : 
                AsmGenUtil.CoerceType(expr7Type, expr6Type);
            
            ConvertTypes(expr6Type, type);
            ConvertTypes(expr7Type, type);
            
            Emit("pop rcx");
            Emit("pop rax");
            Emit("and rax, rcx");
            Emit("push rax");

            return type;
        }
        
        private VariableType Expr7NodeCode(ParseTreeNode node)
        {
            // expr7 -> expr7 eq_type expr8 | expr8
            
            if (node.Children.Count == 1)
            {
                return Expr8NodeCode(node.Children[0]);
            }

            var expr7Type = Expr7NodeCode(node.Children[0]);
            var expr8Type = Expr8NodeCode(node.Children[2]);
            var type = AsmGenUtil.CanCoerceTypes(expr7Type, expr8Type) ? 
                AsmGenUtil.CoerceType(expr7Type, expr8Type) : 
                AsmGenUtil.CoerceType(expr8Type, expr7Type);
            
            ConvertTypes(expr7Type, type);
            ConvertTypes(expr8Type, type);

            Emit("pop rcx");
            Emit("pop rax");
            Emit("cmp rax, rcx");
            Emit("mov rax,  0");
            
            string eqType = EqTypeNodeCode(node.Children[1]);
            if (eqType == "eq")
            {
                Emit("sete al");
            }
            else if (eqType == "neq")
            {
                Emit("setne al");
            }
            else
            {
                throw new Exception("Unknown eq_type: " + eqType);
            }
            
            Emit("push rax");

            return AsmGenUtil.BoolType;
        }
        
        private VariableType Expr8NodeCode(ParseTreeNode node)
        {
            // expr8 -> expr8 cmp_type expr9 | expr9
            
            if (node.Children.Count == 1)
            {
                return Expr9NodeCode(node.Children[0]);
            }
            
            var expr8Type = Expr8NodeCode(node.Children[0]);
            var expr9Type = Expr9NodeCode(node.Children[2]);
            var type = AsmGenUtil.CanCoerceTypes(expr8Type, expr9Type) ? 
                AsmGenUtil.CoerceType(expr8Type, expr9Type) : 
                AsmGenUtil.CoerceType(expr9Type, expr8Type);
            
            ConvertTypes(expr8Type, type);
            ConvertTypes(expr9Type, type);
            
            Emit("pop rcx");
            Emit("pop rax");
            Emit("cmp rax, rcx");
            Emit("mov rax,  0");
            
            string cmpType = CmpTypeNodeCode(node.Children[1]);
            if (cmpType == "GREATER_THAN")
            {
                Emit("setg al");
            }
            else if (cmpType == "LESS_THAN")
            {
                Emit("setl al");
            }
            else if (cmpType == "greater_than_eq")
            {
                Emit("setge al");
            }
            else if (cmpType == "less_than_eq")
            {
                Emit("setle al");
            }
            else
            {
                throw new Exception("Unknown cmp_type: " + cmpType);
            }
            
            Emit("push rax");

            return AsmGenUtil.BoolType;
        }

        private VariableType Expr9NodeCode(ParseTreeNode node)
        {
            // expr9 -> expr9 shift_type expr10 | expr10
            
            if (node.Children.Count == 1)
            {
                return Expr10NodeCode(node.Children[0]);
            }

            var expr9Type = Expr9NodeCode(node.Children[0]);
            var expr10Type = Expr10NodeCode(node.Children[2]);
            var type = AsmGenUtil.CanCoerceTypes(expr9Type, expr10Type) ? 
                AsmGenUtil.CoerceType(expr9Type, expr10Type) : 
                AsmGenUtil.CoerceType(expr10Type, expr9Type);
            
            ConvertTypes(expr9Type, type);
            ConvertTypes(expr10Type, type);

            Emit("pop rcx");
            Emit("pop rax");
            
            string shiftType = ShiftTypeNodeCode(node.Children[1]);
            if (shiftType == "lshift")
            {
                // SAL: Shift arithmetic left
                Emit("sal rax, cl");
            }
            else if (shiftType == "rshift")
            {
                // SAR: Shift arithmetic right
                Emit("sar rax, cl");
            }
            else if (shiftType == "lrotate")
            {
                // ROL: Rotate left
                Emit("rol rax, cl");
            }
            else if (shiftType == "rrotate")
            {
                // ROR: Rotate right
                Emit("ror rax, cl");
            }
            else if (shiftType == "urshift")
            {
                // SHR: Shift logical right
                Emit("shr rax, cl");
            }
            else
            {
                throw new Exception("Unknown shift_type: " + shiftType);
            }
            
            Emit("push rax");
            
            return type;
        }
        
        private VariableType Expr10NodeCode(ParseTreeNode node)
        {
            // expr10 -> expr10 math1_type expr11 | expr11
            
            if (node.Children.Count == 1)
            {
                return Expr11NodeCode(node.Children[0]);
            }

            var expr10Type = Expr10NodeCode(node.Children[0]);
            var expr11Type = Expr11NodeCode(node.Children[2]);
            
            var type = AsmGenUtil.CanCoerceTypes(expr10Type, expr11Type) ? 
                AsmGenUtil.CoerceType(expr10Type, expr11Type) : 
                AsmGenUtil.CoerceType(expr11Type, expr10Type);
            
            ConvertTypes(expr10Type, type);
            ConvertTypes(expr11Type, type);
            
            string mathType = Math1TypeNodeCode(node.Children[1]);
            if (mathType == "ADD")
            {
                // TODO: What do I do if things on the stack are not I64?
                Emit("pop rcx");
                Emit("pop rax");
                Emit("add rax, rcx");
                Emit("push rax");
            }
            else if(mathType == "SUB")
            {
                // TODO: What do I do if things on the stack are not I64?
                Emit("pop rcx");
                Emit("pop rax");
                Emit("sub rax, rcx");
                Emit("push rax");
            }
            else
            {
                throw new Exception("Unknown math1_type: " + mathType);
            }
            
            return type;
        }

        private VariableType Expr11NodeCode(ParseTreeNode node)
        {
            // expr11 -> expr11 math2_type expr12 | expr12
            
            if (node.Children.Count == 1)
            {
                return Expr12NodeCode(node.Children[0]);
            }

            Expr11NodeCode(node.Children[0]);
            string mathType = Math2TypeNodeCode(node.Children[1]);
            Expr12NodeCode(node.Children[2]);
            
            if (mathType == "MUL")
            {
                // TODO: What do I do if things on the stack are not I64?
                Emit("pop rcx");
                Emit("pop rax");
                Emit("imul rax, rcx");
                Emit("push rax");
            }
            else if(mathType == "DIV")
            {
                // TODO: What do I do if things on the stack are not I64?
                Emit("pop rcx");
                Emit("pop rax");
                Emit("mov rdx, 0");
                Emit("idiv rcx");
                Emit("push rax");
            }
            else if(mathType == "MOD")
            {
                // TODO: What do I do if things on the stack are not I64?
                Emit("pop rcx");
                Emit("pop rax");
                Emit("mov rdx, 0");
                Emit("idiv rcx");
                Emit("push rdx");
            }
            else
            {
                throw new Exception("Unknown math2_type: " + mathType);
            }
            
            return null; // TODO: Provide correct type
        }

        private VariableType Expr12NodeCode(ParseTreeNode node)
        {
            // expr12 -> expr13 power expr12 | expr13
            
            if (node.Children.Count == 1)
            {
                return Expr13NodeCode(node.Children[0]);
            }
            
            throw new NotImplementedException("expr12's POWER is not implemented yet!");

            var expr13Type = Expr13NodeCode(node.Children[0]);
            var expr12Type = Expr12NodeCode(node.Children[2]);
            
            return null; // TODO: Provide correct type
        }

        private VariableType Expr13NodeCode(ParseTreeNode node)
        {
            // expr13 -> unary_ops expr13 | cast expr13 | sizeof expr13 | expr14
            
            if (node.Children.Count == 1)
            {
                return Expr14NodeCode(node.Children[0]);
            }

            switch (node.Children[0].Symbol)
            {
                case "unary_ops":
                {
                    string unaryOp = UnaryOpsNodeCode(node.Children[0]);
                    Expr13NodeCode(node.Children[1]);

                    return null; // TODO: Provide correct type
                }
                case "cast":
                {
                    CastNodeCode(node.Children[0]);
                    Expr13NodeCode(node.Children[1]);
                    
                    return null; // TODO: Provide correct type
                }
                case "sizeof":
                {
                    SizeOfNodeCode(node.Children[0]);
                    Expr13NodeCode(node.Children[1]);
                    
                    return null; // TODO: Provide correct type
                }
                default: throw new Exception("Unknown Symbol: " + node.Children[0].Symbol);
            }
        }

        private VariableType Expr14NodeCode(ParseTreeNode node)
        {
            // expr14 -> OCT_LIT | HEX_LIT | BIN_LIT | FLT_LIT | INT_LIT | STR_LIT | CHR_LIT | BLN_LIT | ary_lit | tpl_lit | obj | LP expr RP

            string literalValue = node.Children[0].Token;
            
            switch (node.Children[0].Symbol)
            {
                case "LP":
                {
                    var type = ExprNodeCode(node.Children[1]);

                    return type;
                }
                case "obj":
                {
                    var type = ObjNodeCode(node.Children[0]);

                    return type;
                }
                case "tpl_lit":
                {
                    TplLitNodeCode(node.Children[0]);
                    
                    throw new NotImplementedException("Tuple Literals are not implemented yet.");
                    
                    return new VariableType {
                        BaseType = BasicType.Tuple,
                        Generics = new List<VariableType>(), // TODO: Fill with data
                        TypeName = AsmGenUtil.GetBasicTypeAsString(BasicType.Tuple),
                        Size = -1
                    };
                }
                case "ary_lit":
                {
                    throw new NotImplementedException("Array Literals are not implemented yet.");
                    AryLitNodeCode(node.Children[0]);
                    return new VariableType {
                        BaseType = BasicType.Array,
                        TypeName = AsmGenUtil.GetBasicTypeAsString(BasicType.Array),
                        Size = -1
                    };
                }
                case "OCT_LIT":
                {
                    EmitIntLiteral(LiteralToInteger(literalValue, 8, out var type));

                    return new VariableType {
                        BaseType = type,
                        TypeName = AsmGenUtil.GetBasicTypeAsString(type),
                        Size = AsmGenUtil.GetBasicTypeByteSize(type)
                    };
                }
                case "HEX_LIT":
                {
                    EmitIntLiteral(LiteralToInteger(literalValue, 16, out var type));
                    return new VariableType {
                        BaseType = type,
                        TypeName = AsmGenUtil.GetBasicTypeAsString(type),
                        Size = AsmGenUtil.GetBasicTypeByteSize(type)
                    };
                }
                case "BIN_LIT":
                {
                    EmitIntLiteral(LiteralToInteger(literalValue, 2, out var type));
                    return new VariableType {
                        BaseType = type,
                        TypeName = AsmGenUtil.GetBasicTypeAsString(type),
                        Size = AsmGenUtil.GetBasicTypeByteSize(type)
                    };
                }
                case "INT_LIT":
                {
                    EmitIntLiteral(LiteralToInteger(literalValue, 10, out var type));
                    return new VariableType {
                        BaseType = type,
                        TypeName = AsmGenUtil.GetBasicTypeAsString(type),
                        Size = AsmGenUtil.GetBasicTypeByteSize(type)
                    };
                }
                case "CHR_LIT":
                {
                    throw new NotImplementedException("Char Literals are not implemented yet.");
                    return new VariableType {
                        BaseType = BasicType.U8,
                        TypeName = "CHR",
                        Size = 1
                    };
                }
                case "FLT_LIT":
                {
                    throw new NotImplementedException("Float Literals are not implemented yet.");
                    
                    var type = EmitFloatLiteral(literalValue);
                    return new VariableType {
                        BaseType = type,
                        TypeName = AsmGenUtil.GetBasicTypeAsString(type),
                        Size = AsmGenUtil.GetBasicTypeByteSize(type)
                    };
                }
                case "BLN_LIT":
                {
                    if (literalValue.ToLower().Trim() == "true")
                    {
                        Emit("mov rax, 1");
                        Emit("push rax");
                    }
                    else if (literalValue.ToLower().Trim() == "false")
                    {
                        Emit("mov rax, 0");
                        Emit("push rax");
                    }
                    else
                    {
                        throw new Exception("Should not be possible!");
                    }
                    
                    return new VariableType {
                        BaseType = BasicType.Bool,
                        TypeName = AsmGenUtil.GetBasicTypeAsString(BasicType.Bool),
                        Size = AsmGenUtil.GetBasicTypeByteSize(BasicType.Bool)
                    };
                }
                case "STR_LIT":
                {
                    throw new NotImplementedException("String Literals are not implemented yet.");
                    return new VariableType {
                        BaseType = BasicType.Str,
                        TypeName = AsmGenUtil.GetBasicTypeAsString(BasicType.Str),
                        Size = -1
                    };
                }
                default: throw new Exception("Unknown Symbol: " + node.Children[0].Symbol);
            }
        }

        private static long LiteralToInteger(string lit, int numberBase, out BasicType b)
        {
            // remove any prefixes
            switch (numberBase)
            {
                case 2:
                    lit = lit.Replace("0b", "");
                    break;
                case 8:
                    lit = lit.Replace("0o", "");
                    break;
                case 10: break;
                case 16:
                    lit = lit.Replace("0x", "");
                    break;
                default: throw new Exception("Unknown number base: " + numberBase);
            }
            
            // remove any suffixes
            if (lit.EndsWith("u8"))
            {
                lit = lit.Replace("u8", "");
                b = BasicType.U8;
            }
            else if (lit.EndsWith("u16"))
            {
                lit = lit.Replace("u16", "");
                b = BasicType.U16;
            }
            else if (lit.EndsWith("u32"))
            {
                lit = lit.Replace("u32", "");
                b = BasicType.U32;
            }
            else if (lit.EndsWith("u64"))
            {
                lit = lit.Replace("u64", "");
                b = BasicType.U64;
            }
            else if (lit.EndsWith("i8"))
            {
                lit = lit.Replace("i8", "");
                b = BasicType.I8;
            }
            else if (lit.EndsWith("i16"))
            {
                lit = lit.Replace("i16", "");
                b = BasicType.I16;
            }
            else if (lit.EndsWith("i32"))
            {
                lit = lit.Replace("i32", "");
                b = BasicType.I32;
            }
            else
            {
                lit = lit.Replace("i64", "");
                b = BasicType.I64;
            }
            
            return Convert.ToInt64(lit, numberBase);
        }
        
        private void EmitIntLiteral(long literal)
        {
            Emit("mov rax, {0}", literal);
            Emit("push rax");
        }

        private BasicType EmitFloatLiteral(string lit)
        {
            if (lit.EndsWith("f32") || lit.EndsWith("f"))
            {
                lit = lit.Replace("f32", "").Replace("f", "");

                if (!lit.Contains("."))
                {
                    lit += ".0";
                }

                Emit("mov rax, __float32__({0})", lit);
                Emit("push rax");
                
                return BasicType.F32;
            }

            lit = lit.Replace("f64", "");

            if (!lit.Contains("."))
            {
                lit += ".0";
            }

            Emit("mov rax, __float64__({0})", lit);
            Emit("push rax");
            
            return BasicType.F64;
        }
        
        private void AryLitNodeCode(ParseTreeNode node)
        {
            // ary_lit -> LBRACE RBRACE | LBRACE expr RBRACE | LBRACE expr COMMA expr_lst RBRACE

            if (node.Children.Count > 2)
            {
                ExprNodeCode(node.Children[1]);
                if (node.Children.Count > 3)
                {
                    ExprLstNodeCode(node.Children[3]);
                }
            }
        }
        
        private void TplLitNodeCode(ParseTreeNode node)
        {
            // tpl_lit -> LP expr COMMA expr_lst RP
            
            ExprNodeCode(node.Children[1]);
            ExprLstNodeCode(node.Children[3]);
        }

        private void ExprLstNodeCode(ParseTreeNode node)
        {
            // expr_lst -> expr | expr COMMA expr_lst
            
            ExprNodeCode(node.Children[0]);
            if (node.Children.Count > 1)
            {
                // expr_lst -> expr COMMA expr_lst
                ExprLstNodeCode(node.Children[2]);
            }
        }

        private VariableType ObjNodeCode(ParseTreeNode node, bool location=false)
        {
            // obj -> ID | ID access_id | TYPE | TYPE generics_call func_call | TYPE func_call
            
            if (node.Children[0].Symbol == "TYPE")
            {
                string type = node.Children[0].Token;
                
                var functionSignature = new Function
                {
                    Identifier = type
                };
                
                if (node.Children.Count > 1)
                {
                    if (node.Children[1].Symbol == "generics_call")
                    {
                        // TODO: create & integrate GenericsCallNodeCode results into functionSignature
                        throw new NotImplementedException("Generics are not implemented yet!");
                        
                        GenericsCallNodeCode(node.Children[1]);
                        FuncCallNodeCode(node.Children[2], ref functionSignature);
                    }
                    else
                    {
                        FuncCallNodeCode(node.Children[1], ref functionSignature);
                    }
                }
                
                // TODO: perform function call?
                
                return functionSignature.Type;
            }
            
            string identifier = node.Children[0].Token;
            
            if (node.Children.Count > 1)
            {
                AccessIdNodeCode(node.Children[1], identifier);
                return null;
            }
            
            var variable = GetVariableFromScope(identifier).Item3;

            Emit(location ? "mov rax, {0}" : "mov rax, [{0}]", variable.Identifier);
            Emit("push rax");

            return variable.Type;
        }

        private void AccessIdNodeCode(ParseTreeNode node, string identifier)
        {
            // access_id -> DOT obj |
            //              LBRACE expr RBRACE access_id |
            //              LBRACE expr RBRACE |
            //              generics_call func_call |
            //              func_call
            
            switch (node.Children[0].Symbol)
            {
                case "DOT":
                {
                    ObjNodeCode(node.Children[1]);
                }
                    break;
                case "LBRACE":
                {
                    ExprNodeCode(node.Children[1]);
                    if (node.Children.Count > 3)
                    {
                        AccessIdNodeCode(node.Children[3], identifier);
                    }
                }
                    break;
                case "generics_call":
                {
                    throw new NotImplementedException("Generics are not implemented yet!");
                    
                    GenericsCallNodeCode(node.Children[0]);
                    //FuncCallNodeCode(node.Children[1], identifier);
                }
                    break;
                case "func_call":
                {
                    var functionSignature = new Function { Identifier = identifier };
                    FuncCallNodeCode(node.Children[0], ref functionSignature);

                    if (IsBuiltinFunction(functionSignature.Identifier))
                    {
                        if (functionSignature.Identifier == "print")
                        {
                            string arg0 = AsmGenUtil.GetArgRegister(0);
                            Emit("pop {0}", arg0);
                            Emit("call printI64");
                        }
                        else
                        {
                            throw new NotImplementedException("Builtin function '" + functionSignature.Identifier + "' is not implemented yet!");
                        }
                    }
                    else
                    {
                        throw new NotImplementedException("Custom functions are not implemented yet!");
                    }
                }
                    break;
                default: throw new Exception("Unknown Symbol: " + node.Children[0].Symbol);
            }
        }

        private static string UnaryOpsNodeCode(ParseTreeNode node)
        {
            // unary_ops -> ADD | SUB | LOGICAL_NOT | BITWISE_NOT | NOT | NEW | DELETE
            return node.Children[0].Symbol;
        }
        
        private void CastNodeCode(ParseTreeNode node)
        {
            // cast -> LP type_decl RP
            TypeDeclNodeCode(node.Children[1]);
        }
        
        private static string Math1TypeNodeCode(ParseTreeNode node)
        {
            // math1_type -> ADD | SUB
            return node.Children[0].Symbol;
        }
        
        private static string Math2TypeNodeCode(ParseTreeNode node)
        {
            // math2_type -> MUL | DIV | MOD
            return node.Children[0].Symbol;
        }
        
        private void SizeOfNodeCode(ParseTreeNode node)
        {
            // sizeof -> SIZEOF LP type_decl RP
            TypeDeclNodeCode(node.Children[2]);
        }
        
        private static string ShiftTypeNodeCode(ParseTreeNode node)
        {
            // shift_type -> lshift | rshift | lrotate | rrotate | urshift
            return node.Children[0].Symbol;
        }
        
        private static string CmpTypeNodeCode(ParseTreeNode node)
        {
            // cmp_type -> GREATER_THAN | LESS_THAN | greater_than_eq | less_than_eq
            return node.Children[0].Symbol;
        }
        
        private static string EqTypeNodeCode(ParseTreeNode node)
        {
            // eq_type -> eq | neq
            return node.Children[0].Symbol;
        }
        
        private static string AssignmentTypeNodeCode(ParseTreeNode node)
        {
            // assignment_type -> ASSIGN | pow_assign | add_assign | sub_assign | mul_assign | div_assign | bit_or_assign | bit_not_assign | bit_and_assign | bit_xor_assign | lshift_assign | rshift_assign | mod_assign | lrotate_assign | rrotate_assign | urshift_assign
            return node.Children[0].Symbol;
        }
        
        private void EnumDeclNodeCode(ParseTreeNode node)
        {
            // enum_decl -> ENUM ID LCURLY enum_statements RCURLY |
            //              ENUM ID LCURLY RCURLY |
            //              ENUM ID COLON TYPE LCURLY enum_statements RCURLY |
            //              ENUM ID COLON TYPE LCURLY RCURLY

            string identifier = node.Children[1].Token;
            string enumElementsSizedAs = node.Children[2].Symbol == "COLON" ? node.Children[3].Token : "UNKNOWN";

            if (enumElementsSizedAs == "UNKNOWN" && node.Children[3].Symbol == "enum_statements")
            {
                EnumStatementsNodeCode(node.Children[3]);
            }
            else if (node.Children[5].Symbol == "enum_statements")
            {
                EnumStatementsNodeCode(node.Children[5]);
            }
        }

        private void EnumStatementsNodeCode(ParseTreeNode node)
        {
            // enum_statements -> ID |
            //                    ID COMMA
            //                    ID COMMA enum_statements |
            //                    ID ASSIGN expr1 |
            //                    ID ASSIGN expr1 COMMA |
            //                    ID ASSIGN expr1 COMMA enum_statements |
            
            string identifier = node.Children[0].Token;
                
            if (node.Children.Count > 1)
            {
                if (node.Children[1].Symbol == "COMMA" && node.Children.Count > 2)
                {
                    EnumStatementsNodeCode(node.Children[2]);
                }
                else if(node.Children.Count > 2)
                {
                    Expr1NodeCode(node.Children[2]);
                    if (node.Children.Count > 4)
                    {
                        EnumStatementsNodeCode(node.Children[4]);
                    }
                }
            }
        }

        private void StructOrUnionDeclNodeCode(ParseTreeNode node)
        {
            // struct_or_union_decl -> union_or_struct ID LCURLY RCURLY |
            //                         union_or_struct ID LCURLY union_or_struct_statements RCURLY

            string unionOrStruct = UnionOrStructNodeCode(node.Children[0]);
            string identifier = node.Children[1].Token;
            
            if (node.Children.Count == 5)
            {
                UnionOrStructStatementsNodeCode(node.Children[3]);
            }
        }
        
        private void AnonymousStructOrUnionDeclNodeCode(ParseTreeNode node)
        {
            // anonymous_struct_or_union_decl -> union_or_struct LCURLY RCURLY |
            //                                   union_or_struct LCURLY union_or_struct_statements RCURLY

            string unionOrStruct = UnionOrStructNodeCode(node.Children[0]);
            if (node.Children.Count == 4)
            {
                UnionOrStructStatementsNodeCode(node.Children[2]);
            }
        }

        private static string UnionOrStructNodeCode(ParseTreeNode node)
        {
            // union_or_struct -> STRUCT | UNION
            return node.Children[0].Symbol;
        }

        private void UnionOrStructStatementsNodeCode(ParseTreeNode node)
        {
            // union_or_struct_statements -> union_or_struct_statement union_or_struct_statements | union_or_struct_statement
            
            UnionOrStructStatementNodeCode(node.Children[0]);
            if (node.Children.Count == 2)
            {
                UnionOrStructStatementsNodeCode(node.Children[1]);
            }
        }

        private void UnionOrStructStatementNodeCode(ParseTreeNode node)
        {
            // union_or_struct_statement -> ID COLON type_decl SEMI |
            //                              ID COLON type_decl size_decl SEMI |
            //                              anonymous_struct_or_union_decl
            
            switch (node.Children.Count)
            {
                case 1:
                    AnonymousStructOrUnionDeclNodeCode(node.Children[0]);
                    return;
                case 4:
                {
                    string identifier = node.Children[0].Token;
                    TypeDeclNodeCode(node.Children[2]);
                }
                    break;
                case 5:
                {
                    string identifier = node.Children[0].Token;
                    string size = SizeDeclNodeCode(node.Children[3]);
                    TypeDeclNodeCode(node.Children[2]);
                    
                }
                    break;
                default: throw new Exception("Unknown child count: " + node.Children.Count);
            }
        }

        private static string SizeDeclNodeCode(ParseTreeNode node)
        {
            // size_decl -> BITS INT_LIT
            return node.Children[1].Token;
        }
        
        private VariableType StructInitializerNodeCode(ParseTreeNode node)
        {
            // struct_initializer -> ID LCURLY RCURLY | ID LCURLY struct_initializer_statements RCURLY
            
            string identifier = node.Children[0].Token;
            if (node.Children.Count == 4)
            {
                StructInitializerStatementsNodeCode(node.Children[2]);
            }

            return null;
        }

        private void StructInitializerStatementsNodeCode(ParseTreeNode node)
        {
            // struct_initializer_statements -> DOT ID ASSIGN expr COMMA struct_initializer_statements |
            //                                  DOT ID ASSIGN expr COMMA |
            //                                  DOT ID ASSIGN expr
            
            string identifier = node.Children[1].Token;
            ExprNodeCode(node.Children[3]);
            
            if (node.Children.Count == 6)
            {
                StructInitializerStatementsNodeCode(node.Children[5]);
            }
        }

        private void InterfaceDeclNodeCode(ParseTreeNode node)
        {
            // interface_decl -> INTERFACE ID LCURLY RCURLY | INTERFACE ID LCURLY interface_statements RCURLY
            
            string identifier = node.Children[1].Token;
            
            if (node.Children.Count == 5)
            {
                InterfaceStatementsNodeCode(node.Children[3]);
            }
        }

        private void InterfaceStatementsNodeCode(ParseTreeNode node)
        {
            // interface_statements -> interface_statement interface_statements | interface_statement
            
            InterfaceStatementNodeCode(node.Children[0]);
                
            if (node.Children.Count == 2)
            {
                InterfaceStatementsNodeCode(node.Children[1]);
            }
        }

        private void InterfaceStatementNodeCode(ParseTreeNode node)
        {
            // interface_statement -> variable_decl |
            //                        func_proto_decl |
            //                        PUBLIC func_proto_decl |
            //                        PUBLIC ABSTRACT func_proto_decl |
            //                        PUBLIC DEFAULT func_decl |
            //                        PUBLIC STATIC func_decl |
            //                        DEFAULT func_decl |
            //                        STATIC func_decl |
            //                        PRIVATE func_decl |
            //                        PRIVATE STATIC func_decl

            switch (node.Children[0].Symbol)
            {
                case "variable_decl":
                    VariableDeclNodeCode(node.Children[0]);
                    break;
                case "func_proto_decl":
                    FuncProtoDeclNodeCode(node.Children[0]);
                    break;
                case "PUBLIC":
                {
                    if (node.Children.Count == 2)
                    {
                        FuncProtoDeclNodeCode(node.Children[1]);
                    }
                    else
                    {
                        string extraPrefix = node.Children[1].Symbol;
                        
                        if(extraPrefix == "ABSTRACT")
                        {
                            FuncProtoDeclNodeCode(node.Children[2]);
                        }
                        else
                        {
                            FuncDeclNodeCode(node.Children[2]);
                        }
                    }
                }
                    break;
                case "DEFAULT":
                    FuncDeclNodeCode(node.Children[1]);
                    break;
                case "STATIC":
                    FuncDeclNodeCode(node.Children[1]);
                    break;
                case "PRIVATE":
                {
                    if (node.Children.Count == 2)
                    {
                        FuncDeclNodeCode(node.Children[1]);
                    }
                    else
                    {
                        string extraPrefix = node.Children[1].Symbol; // always STATIC at the moment
                        FuncDeclNodeCode(node.Children[2]);
                    }
                }
                    break;
                default: throw new Exception("Unknown Symbol: " + node.Children[0].Symbol);
            }
        }
        
        private void ClassDeclNodeCode(ParseTreeNode node)
        {
            // class_decl -> CLASS ID LCURLY RCURLY |
            //               CLASS ID LCURLY class_statements RCURLY |
            //               CLASS ID class_mods LCURLY RCURLY |
            //               CLASS ID class_mods LCURLY class_statements RCURLY |
            //               CLASS ID class_template_decl LCURLY RCURLY |
            //               CLASS ID class_template_decl LCURLY class_statements RCURLY |
            //               CLASS ID class_template_decl class_mods LCURLY class_statements RCURLY |
            //               CLASS ID class_template_decl class_mods LCURLY RCURLY |
            //               ABSTRACT CLASS ID LCURLY class_statements RCURLY |
            //               ABSTRACT CLASS ID LCURLY RCURLY |
            //               ABSTRACT CLASS ID class_mods LCURLY class_statements RCURLY |
            //               ABSTRACT CLASS ID class_mods LCURLY RCURLY |
            //               ABSTRACT CLASS ID class_template_decl LCURLY class_statements RCURLY |
            //               ABSTRACT CLASS ID class_template_decl LCURLY RCURLY |
            //               ABSTRACT CLASS ID class_template_decl class_mods LCURLY class_statements RCURLY |
            //               ABSTRACT CLASS ID class_template_decl class_mods LCURLY RCURLY

            int baseIndex = node.Children[0].Symbol == "CLASS" ? 0 : 1;
            bool isAbstract = baseIndex == 1;
            // a pseudo non-terminal exists here called 'opt_abstract'
            // it isn't actually in the grammar, it's just a logical creation to reduce code duplication
            
            string identifier = node.Children[baseIndex + 1].Token;
            switch (node.Children[baseIndex + 2].Symbol)
            {
                case "LCURLY" when node.Children.Count == baseIndex + 4:
                {
                    // class_decl -> opt_abstract CLASS ID LCURLY RCURLY
                    
                }
                    break;
                case "LCURLY":
                {
                    // class_decl -> opt_abstract CLASS ID LCURLY class_statements RCURLY
                    ClassStatementsNodeCode(node.Children[baseIndex + 3]);
                    
                }
                    break;
                case "class_mods":
                {
                    ClassModsNodeCode(node.Children[baseIndex + 2]);
                
                    if (node.Children.Count == baseIndex + 5)
                    {
                        // class_decl -> opt_abstract CLASS ID class_mods LCURLY RCURLY
                    
                    }
                    else
                    {
                        // class_decl -> opt_abstract CLASS ID class_mods LCURLY class_statements RCURLY
                        ClassStatementsNodeCode(node.Children[baseIndex + 4]);
                    }

                    break;
                }

                case "class_template_decl":
                {
                    ClassTemplateDeclNodeCode(node.Children[baseIndex + 2]);
                    if (node.Children[baseIndex + 3].Symbol == "LCURLY")
                    {
                        if (node.Children.Count == baseIndex + 5)
                        {
                            // class_decl -> opt_abstract CLASS ID class_template_decl LCURLY RCURLY
                        
                        }
                        else
                        {
                            // class_decl -> opt_abstract CLASS ID class_template_decl LCURLY class_statements RCURLY
                        
                            ClassStatementsNodeCode(node.Children[baseIndex + 4]);
                        }
                    }
                    else
                    {
                        ClassModsNodeCode(node.Children[baseIndex + 3]);
                        if (node.Children.Count == baseIndex + 6)
                        {
                            // class_decl -> opt_abstract CLASS ID class_template_decl class_mods LCURLY RCURLY
                        
                        }
                        else
                        {
                            // class_decl -> opt_abstract CLASS ID class_template_decl class_mods LCURLY class_statements RCURLY
                            ClassStatementsNodeCode(node.Children[baseIndex + 5]);
                        }
                    }

                    break;
                }

                default:
                    throw new Exception("Unknown Symbol: " + node.Children[baseIndex + 2].Symbol);
            }
        }
        
        private void ClassModsNodeCode(ParseTreeNode node)
        {
            // class_mods -> INHERITS class_mod_id_list |
            //               INHERITS class_mod_id_list COMMA IMPLEMENTS class_mod_id_list |
            //               IMPLEMENTS class_mod_id_list
            
            if (node.Children[0].Symbol == "INHERITS")
            {
                if (node.Children.Count > 2)
                {
                    // have inherits list & interface list
                    ClassModIdListNodeCode(node.Children[1]);
                    ClassModIdListNodeCode(node.Children[4]);
                }
                else
                {
                    // have just inherits list
                    ClassModIdListNodeCode(node.Children[1]);
                }
            }
            else
            {
                // just interface list
                ClassModIdListNodeCode(node.Children[1]);
            }
        }

        private void ClassModIdListNodeCode(ParseTreeNode node)
        {
            // class_mod_id_list -> ID AND class_mod_id_list | ID
            
            string identifier = node.Children[0].Token;

            if (node.Children.Count > 1)
            {
                ClassModIdListNodeCode(node.Children[2]);
            }
        }

        private void ClassStatementsNodeCode(ParseTreeNode node)
        {
            // class_statements -> class_statement class_statements | class_statement
            
            ClassStatementNodeCode(node.Children[0]);
                
            if (node.Children.Count > 1)
            {
                ClassStatementsNodeCode(node.Children[1]);
            }
        }

        private static string BasicClassAccessSpecifierNodeCode(ParseTreeNode node)
        {
            // basic_class_access_specifier -> PUBLIC | PRIVATE | PROTECTED
            return node.Children[0].Symbol;
        }

        private static Tuple<string, string> ClassAccessSpecifierNodeCode(ParseTreeNode node)
        {
            // class_access_specifier -> basic_class_access_specifier | STATIC | basic_class_access_specifier STATIC
            
            string basicSpecifier = node.Children[0].Symbol == "basic_class_access_specifier" ? 
                BasicClassAccessSpecifierNodeCode(node.Children[0]) : node.Children[0].Symbol;
            string additionalSpecifier = node.Children.Count > 1 ? node.Children[1].Symbol : "";
            
            return new Tuple<string, string>(basicSpecifier, additionalSpecifier);
        }
        
        private void ClassTemplateDeclNodeCode(ParseTreeNode node)
        {
            // class_template_decl -> LESS_THAN template_id_list GREATER_THAN
            TemplateIdListNodeCode(node.Children[1]);
        }

        private void TemplateIdListNodeCode(ParseTreeNode node)
        {
            // template_id_list -> ID COMMA template_id_list | ID
            
            string identifier = node.Children[0].Token;

            if (node.Children.Count > 1)
            {
                TemplateIdListNodeCode(node.Children[2]);
            }
        }

        private static string ClassFuncSpecifierNodeCode(ParseTreeNode node)
        {
            // class_func_specifier -> VIRTUAL | OVERRIDE
            return node.Children[0].Symbol;
        }

        private void ClassStatementNodeCode(ParseTreeNode node)
        {
            // class_statement -> class_property |
            //                    class_init_decl |
            //                    class_deinit_decl |
            //                    variable_decl |
            //                    func_decl |
            //                    ABSTRACT func_proto_decl |
            //                    class_access_specifier class_property |
            //                    class_access_specifier variable_decl |
            //                    class_access_specifier func_decl |
            //                    class_access_specifier class_func_specifier func_decl |
            //                    basic_class_access_specifier class_init_decl |
            //                    basic_class_access_specifier class_operator_def |
            //                    basic_class_access_specifier class_roperator_def |
            //                    class_operator_def |
            //                    class_roperator_def

            switch (node.Children[0].Symbol)
            {
                case "class_property":
                {
                    ClassPropertyNodeCode(node.Children[0]);
                }
                    break;
                case "class_init_decl":
                {
                    ClassInitDeclNodeCode(node.Children[0]);
                }
                    break;
                case "class_deinit_decl":
                {
                    ClassDeinitDeclNodeCode(node.Children[0]);
                }
                    break;
                case "variable_decl":
                {
                    VariableDeclNodeCode(node.Children[0]);
                }
                    break;
                case "func_decl":
                {
                    FuncDeclNodeCode(node.Children[0]);
                }
                    break;
                case "ABSTRACT":
                {
                    FuncProtoDeclNodeCode(node.Children[1]);
                }
                    break;
                case "class_access_specifier":
                {
                    (string basicAccessor, string optAdditionalAccessor) = ClassAccessSpecifierNodeCode(node.Children[0]);
                    
                    switch (node.Children[1].Symbol)
                    {
                        case "class_property":
                        {
                            ClassPropertyNodeCode(node.Children[1]);                            
                        }
                            break;
                        case "variable_decl":
                        {
                            VariableDeclNodeCode(node.Children[1]);
                        }
                            break;
                        case "func_decl":
                        {
                            FuncDeclNodeCode(node.Children[1]);
                        }
                            break;
                        case "class_func_specifier":
                        {
                            string virtualOrOverride = ClassFuncSpecifierNodeCode(node.Children[1]);
                            FuncDeclNodeCode(node.Children[2]);
                        }
                            break;
                        default: throw new Exception("Unknown Symbol: " + node.Children[1].Symbol);
                    }
                }
                    break;
                case "basic_class_access_specifier":
                {
                    string basicAccessor = BasicClassAccessSpecifierNodeCode(node.Children[0]);
                    
                    switch (node.Children[1].Symbol)
                    {
                        case "class_init_decl":
                        {
                            ClassInitDeclNodeCode(node.Children[1]);
                        }
                            break;
                        case "class_operator_def":
                        {
                            ClassOperatorDefNodeCode(node.Children[1]);
                        }
                            break;
                        case "class_roperator_def":
                        {
                            ClassRoperatorDefNodeCode(node.Children[1]);
                        }
                            break;
                        default: throw new Exception("Unknown Symbol: " + node.Children[1].Symbol);
                    }
                }
                    break;
                case "class_operator_def":
                {
                    ClassOperatorDefNodeCode(node.Children[0]);
                }
                    break;
                case "class_roperator_def":
                {
                    ClassRoperatorDefNodeCode(node.Children[0]);
                }
                    break;
                default: throw new Exception("Unknown Symbol: " + node.Children[0].Symbol);
            }
        }
        
        private void ClassInitDeclNodeCode(ParseTreeNode node)
        {
            // class_init_decl -> FN INIT LP func_decl_args RP block | FN INIT LP RP block
            
            if (node.Children.Count == 6)
            {
                FuncDeclArgsNodeCode(node.Children[3]);
                BlockNodeCode(node.Children[5]);
            }
            else
            {
                BlockNodeCode(node.Children[4]);
            }
        }
        
        private void ClassDeinitDeclNodeCode(ParseTreeNode node)
        {
            // class_deinit_decl -> FN DEINIT block
            
            BlockNodeCode(node.Children[2]);
        }
        
        private void ClassPropertyNodeCode(ParseTreeNode node)
        {
            // class_property -> ID COLON type_decl LCURLY class_property_def RCURLY |
            //                   ID LCURLY class_property_def RCURLY

            string identifier = node.Children[0].Token;
            if (node.Children.Count == 6)
            {
                TypeDeclNodeCode(node.Children[2]);
                ClassPropertyDefNodeCode(node.Children[4]);
            }
            else
            {
                ClassPropertyDefNodeCode(node.Children[2]);
            }
        }
        
        private void ClassPropertyDefNodeCode(ParseTreeNode node)
        {
            // class_property_def -> GET block |
            //                       GET block SET block |
            //                       SET block |
            //                       SET block GET block
            
            if (node.Children[0].Symbol == "GET")
            {
                // GET
                BlockNodeCode(node.Children[1]);
                
                if (node.Children.Count > 2)
                {
                    // SET
                    BlockNodeCode(node.Children[3]);    
                }
            }
            else
            {
                // SET
                BlockNodeCode(node.Children[1]);
                
                if (node.Children.Count > 2)
                {
                    // GET
                    BlockNodeCode(node.Children[3]);    
                }
            }
        }
        
        private void ClassOperatorDefNodeCode(ParseTreeNode node)
        {
            // class_operator_def -> FN OPERATOR operator_type LP func_decl_args RP fn_rtn type_decl block |
            //                       FN OPERATOR operator_type LP func_decl_args RP block |
            //                       FN OPERATOR operator_type LP RP fn_rtn type_decl block |
            //                       FN OPERATOR operator_type LP RP block
            
            string opType = OperatorTypeNodeCode(node.Children[2]);

            if (node.Children[4].Symbol == "RP")
            {
                if (node.Children.Count == 6)
                {
                    // class_operator_def -> FN OPERATOR operator_type LP RP block
                    BlockNodeCode(node.Children[5]);
                }
                else
                {
                    // class_operator_def -> FN OPERATOR operator_type LP RP fn_rtn type_decl block
                    TypeDeclNodeCode(node.Children[5]);
                    BlockNodeCode(node.Children[6]);   
                }
            }
            else
            {
                FuncDeclArgsNodeCode(node.Children[4]);
                if (node.Children.Count == 7)
                {
                    // class_operator_def -> FN OPERATOR operator_type LP func_decl_args RP block
                    BlockNodeCode(node.Children[6]);
                }
                else
                {
                    // class_operator_def -> FN OPERATOR operator_type LP func_decl_args RP fn_rtn type_decl block
                    TypeDeclNodeCode(node.Children[7]);
                    BlockNodeCode(node.Children[8]);   
                }
            }
        }
        
        private void ClassRoperatorDefNodeCode(ParseTreeNode node)
        {
            // class_roperator_def -> FN ROPERATOR roperator_type LP func_decl_args RP fn_rtn type_decl block |
            //                        FN ROPERATOR roperator_type LP RP fn_rtn type_decl block |
            //                        FN ROPERATOR roperator_type LP func_decl_args RP block |
            //                        FN ROPERATOR roperator_type LP RP block
            
            string rOpType = RoperatorTypeNodeCode(node.Children[2]);
            
            if (node.Children[4].Symbol == "RP")
            {
                if (node.Children.Count == 6)
                {
                    // class_roperator_def -> FN ROPERATOR roperator_type LP RP block
                    BlockNodeCode(node.Children[5]);
                }
                else
                {
                    // class_roperator_def -> FN ROPERATOR roperator_type LP RP fn_rtn type_decl block
                    TypeDeclNodeCode(node.Children[5]);
                    BlockNodeCode(node.Children[6]);   
                }
            }
            else
            {
                FuncDeclArgsNodeCode(node.Children[4]);
                if (node.Children.Count == 7)
                {
                    // class_roperator_def -> FN ROPERATOR roperator_type LP func_decl_args RP block
                    BlockNodeCode(node.Children[6]);
                }
                else
                {
                    // class_roperator_def -> FN ROPERATOR roperator_type LP func_decl_args RP fn_rtn type_decl block
                    TypeDeclNodeCode(node.Children[7]);
                    BlockNodeCode(node.Children[8]);
                }
            }
        }
        
        private string OperatorTypeNodeCode(ParseTreeNode node)
        {
            // operator_type -> LBRACE RBRACE | LBRACE RBRACE ASSIGN | LP RP | BITWISE_NOT | LOGICAL_NOT | ASSIGN | pow_assign | add_assign | sub_assign | mul_assign | div_assign | bit_or_assign | bit_and_assign | bit_not_assign | bit_xor_assign | lshift_assign | rshift_assign | mod_assign | ADD | SUB | MUL | DIV | MOD | power | BITWISE_AND | BITWISE_OR | BITWISE_XOR | lshift | rshift | logical_and | logical_or | GREATER_THAN | LESS_THAN | greater_than_eq | less_than_eq | eq | neq | lrotate | rrotate | urshift | lrotate_assign | rrotate_assign | urshift_assign
            
            switch (node.Children[0].Symbol)
            {
                case "LBRACE":
                    return node.Children.Count == 2 ? "[]" : "[]=";
                case "LP":
                    return "()";
                default:
                    return node.Children[0].Symbol;
            }
        }
        
        private string RoperatorTypeNodeCode(ParseTreeNode node)
        {
            // roperator_type -> ADD | SUB | MUL | DIV | MOD | power | BITWISE_AND | BITWISE_OR | BITWISE_XOR | lshift | rshift | logical_and | logical_or | GREATER_THAN | LESS_THAN | greater_than_eq | less_than_eq | eq | neq | lrotate | rrotate | urshift
            
            return node.Children[0].Symbol;
        }
        
        /* Grammar Productions always without implementations:
         *     - fn_rtn
         *     - let_or_var
         *     - power
         *     - lrotate
         *     - rrotate
         *     - urshift
         *     - lshift
         *     - rshift
         *     - greater_than_eq
         *     - less_than_eq
         *     - eq
         *     - neq
         *     - logical_or
         *     - logical_and
         *     - add_assign
         *     - sub_assign
         *     - mul_assign
         *     - mod_assign
         *     - div_assign
         *     - bit_or_assign
         *     - bit_and_assign
         *     - bit_not_assign
         *     - bit_xor_assign
         *     - lrotate_assign
         *     - rrotate_assign
         *     - urshift_assign
         */
    }
}
