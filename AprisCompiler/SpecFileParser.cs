using System;
using System.Collections.Generic;
using System.IO;

namespace AprisCompiler
{
    public class SpecFileParser
    {
        public readonly NonTerminal StartSymbol;
        public readonly Dictionary<string, Terminal> Tokens = new Dictionary<string, Terminal>();
        public readonly Dictionary<string, NonTerminal> Productions = new Dictionary<string, NonTerminal>();
        public readonly List<Terminal> OrderedTokens = new List<Terminal>();
        
        public SpecFileParser(string filePath, bool addWhiteSpace=false)
        {
            if (!File.Exists(filePath))
            {
                throw new Exception("Spec File Parse Error! Spec File does not exist at the given path.");
            }

            bool builtInWhiteSpace = false;
            using (StreamReader file = new StreamReader(filePath))
            {
                bool tokenStage = true;
                string line;
                int lineNum = 0;
                while((line = file.ReadLine()) != null)
                {
                    line = line.Trim();
                    lineNum++;
                    if (line.Length == 0)
                    {
                        tokenStage = false;
                        continue;
                    }

                    // spec file comment
                    if (line.StartsWith("#"))
                    {
                        continue;
                    }
                    
                    string[] sides = line.Split(new [] {"->"}, StringSplitOptions.None);

                    if (sides.Length != 2)
                    {
                        throw new Exception("Spec File Parse Error! Number of Post '->' split strings != 2. On line #: " + lineNum);
                    }

                    sides[0] = sides[0].Trim();
                    sides[1] = sides[1].Trim();

                    if (sides[0].Length == 0 || sides[1].Length == 0)
                    {
                        throw new Exception("Spec File Parse Error! Unknown line format. On line #: " + lineNum);
                    }
                    
                    if (tokenStage)
                    {
                        // Example: LPAREN -> [(]
                        if (!builtInWhiteSpace && sides[0].ToLower() == "whitespace")
                        {
                            builtInWhiteSpace = true;
                        }

                        var t = new Terminal(sides[0], sides[1]);
                        Tokens.Add(sides[0], t);
                        OrderedTokens.Add(t);
                    }
                    else
                    {
                        // Example: S -> LPAREN S RPAREN | lambda                        
                        List<List<string>> rhs = new List<List<string>>();
                        string[] subProductions = sides[1].Split('|');
                        foreach (string subProduction in subProductions)
                        {
                            string[] parts = subProduction.Trim().Split(' ');
                            List<string> subRhs = new List<string>();
                            foreach (string p in parts)
                            {
                                string part = p.Trim();
                                if (part.ToLower() == "lambda")
                                {
                                    if (parts.Length > 1)
                                    {
                                        throw new Exception("Spec File Parse Error! More than just 'lambda' in a lambda production. On line #: " + lineNum);
                                    }
                                    break;
                                }
                                subRhs.Add(part);
                            }
                            rhs.Add(subRhs);
                        }

                        NonTerminal n = new NonTerminal(sides[0], rhs);
                        if (StartSymbol == null)
                        {
                            StartSymbol = n;
                        }

                        if (Productions.ContainsKey(sides[0]))
                        {
                            throw new Exception("Error, Duplicate Grammar Item: " + sides[0]);
                        }
                        
                        Productions.Add(sides[0], n);
                    }
                }
            }
            
            if (addWhiteSpace && !builtInWhiteSpace)
            {
                var t = new Terminal("WHITESPACE", "\\s");
                Tokens.Add("WHITESPACE", t);
                OrderedTokens.Add(t);
            }
            
            Verify();
        }

        private void Verify()
        {
            foreach (var production in Productions)
            {
                foreach (Production subProduction in production.Value.Rhs)
                {
                    foreach (string item in subProduction.Rhs)
                    {
                        if (!Tokens.ContainsKey(item) && !Productions.ContainsKey(item))
                        {
                            throw new Exception("Error, Unknown Grammar Item: " + item);
                        }
                    }
                }
            }
        }

        public override string ToString()
        {
            string str = "Spec File Contents:\n";
            
            foreach (var pair in Tokens)
            {
                str += "\t" + pair.Value + "\n";
            }

            str += "\n";
            
            foreach (var pair in Productions)
            {
                str += "\t" + pair.Value + "\n";
            }

            return str.Substring(0, str.Length - 1);
        }
    }
}
