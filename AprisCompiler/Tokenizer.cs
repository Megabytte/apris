using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AprisCompiler
{
    public class Token
    {
        public readonly string Type;
        public readonly string Lexeme;
        public readonly int Line;
        public readonly int Column;

        public Token(string type, string lexeme, int line, int column)
        {
            Type = type;
            Lexeme = lexeme;
            Line = line;
            Column = column;
        }

        public override string ToString()
        {
            return "Token<Type:" + Type + ",Lexeme:\"" + Lexeme + "\",Line:" + Line + ",Column:" + Column + ">";
        }
    }
    
    public class Tokenizer
    {
        public readonly List<Terminal> Tokens;
        
        public Tokenizer(List<Terminal> tokens)
        {
            Tokens = tokens;
        }

        public List<Token> Tokenize(string filePath, bool removeWhitespace=false)
        {
            if (!File.Exists(filePath))
            {
                throw new Exception("Tokenization Error! Input file does not exist at the given path.");
            }

            var tokenStream = new List<Token>();

            int filePosition = 0;
            int column = 1;
            int line = 1;
            var rawContents = File.ReadAllText(filePath);
            string commentlessContents = RemoveAprisComments(rawContents);
            while (filePosition < commentlessContents.Length)
            {
                bool success = false;
                foreach (var definition in Tokens)
                {
                    var match = definition.Rhs.Match(commentlessContents, filePosition);
                    if (match.Success && match.Index == filePosition && match.Length != 0)
                    {
                        success = true;
                        filePosition += match.Length;
                        
                        tokenStream.Add(new Token(definition.Lhs, match.Value, line, column));
                        column += match.Length;
                        
                        int postLine = line;
                        line += match.Value.Count(c => c == '\n');
                        if (postLine != line)
                        {
                            column = 1;
                        }
                        break;
                    }
                }

                if (!success)
                {
                    throw new Exception("Tokenization Error! Could not match input at Line: " + line + " Column: " + column + " with any token.");
                }
            }

            if (removeWhitespace)
            {
                return tokenStream.Where(x => x.Type.ToLower() != "whitespace").ToList();
            }

            return tokenStream;
        }

        private string RemoveAprisComments(string aprisFileContents)
        {
            // Comment Types in Apris:
            //     //      C++ Style    - Comment
            //     ///     C++ Style    - Doc Comment : (Case Handled By Normal Comment)
            //     #       Python Style - Comment
            //     ##      Python Style - Doc Comment : (Case Handled By Normal Comment)
            //     /* */   C Style      - Comment
            //     /** **/ C Style      - Doc Comment : (Case Handled By Normal Comment)
            // Note: "/* /* */ */" Nested Comments are ALLOWED!
            
            var singleLineComments = new Regex(@"(\/\/.*)|(#.*)", RegexOptions.Compiled | RegexOptions.Multiline);

            aprisFileContents = RemoveMultiLineComments(aprisFileContents);
            
            return singleLineComments.Replace(aprisFileContents, "");
        }

        private static string RemoveMultiLineComment(string contents, int startLocation, int endLocation)
        {
            var builder = new StringBuilder();
            builder.Append(contents.Substring(0, startLocation));

            string cmt = contents.Substring(startLocation, endLocation - startLocation);
            foreach (char c in cmt)
            {
                if (!(c == '\n' || c == '\r'))
                {
                    builder.Append(" ");
                }
                else
                {
                    builder.Append(c);
                }
            }

            builder.Append(contents.Substring(endLocation, contents.Length - endLocation));

            return builder.ToString();
        }
        
        private static string RemoveMultiLineComments(string contents)
        {
            int line = 1;
            int column = 1;
            
            int nestCounter = 0;
            char lastC = ' ';
            
            int startLocationLine = -1;
            int startLocationColumn = -1;
            int startLocation = -1;

            for (int i = 0; i < contents.Length; i++)
            {
                char thisC = contents[i];

                if (lastC == '/' && thisC == '*')
                {
                    if (nestCounter == 0)
                    {
                        startLocation = i - 1;
                    }
                    nestCounter++;
                    startLocationLine = line;
                    startLocationColumn = column - 1;
                }
                else if (lastC == '*' && thisC == '/')
                {
                    nestCounter--;
                    if (nestCounter == 0)
                    {
                        contents = RemoveMultiLineComment(contents, startLocation, i + 1);
                    }

                    if (nestCounter < 0)
                    {
                        throw new Exception("Closing Unopened Multiline Comment At Line: " + line + " Column: " + (column - 1));
                    }
                }

                if (thisC == '\n')
                {
                    line++;
                    column = 1;
                }
                else
                {
                    column++;
                }

                lastC = thisC;
            }

            if (nestCounter != 0)
            {
                throw new Exception("Unclosed Multiline Comment At Line: " + startLocationLine + " Column: " + startLocationColumn);
            }

            return contents;
        }
    }
}
