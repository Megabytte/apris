using System;
using System.Text.RegularExpressions;

namespace AprisCompiler
{
    public class Terminal : IEquatable<Terminal>
    {
        public readonly string Lhs;
        public readonly Regex Rhs;
        
        public Terminal(string lhs, string rhs)
        {
            Lhs = lhs;
            Rhs = new Regex(rhs, RegexOptions.Compiled | RegexOptions.Multiline);
        }

        public bool Equals(Terminal other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(Lhs, other.Lhs) && Equals(Rhs, other.Rhs);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Terminal) obj);
        }

        public override int GetHashCode()
        {
            unchecked // just ignores integer overflows
            {
                return ((Lhs != null ? Lhs.GetHashCode() : 0) * 397) ^ (Rhs != null ? Rhs.GetHashCode() : 0);
            }
        }

        public override string ToString()
        {
            return Lhs + " -> " + Rhs;
        }
    }
}
