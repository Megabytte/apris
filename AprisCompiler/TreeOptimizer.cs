using System;
using System.Collections.Generic;

namespace AprisCompiler
{
    public class TreeOptimizerRule : IComparable<TreeOptimizerRule>
    {
        public int Priority;
        public ParseTreeNode Lhs;
        public ParseTreeNode Rhs;

        public delegate bool RuleCallback(ParseTreeNode replaced);

        private RuleCallback Callback;

        public TreeOptimizerRule(int p, ParseTreeNode lhs, ParseTreeNode rhs, RuleCallback cb)
        {
            Priority = p;
            Lhs = lhs;
            Rhs = rhs;
            Callback = cb;
        }
        
        public int CompareTo(TreeOptimizerRule other)
        {
            return Priority.CompareTo(other.Priority);
        }

        private List<ParseTreeNode> Find(ParseTreeNode root)
        {
            var found = new List<ParseTreeNode>();
            
            var stack = new Stack<ParseTreeNode>();
            stack.Push(root);
            
            while (stack.Count > 0)
            {
                var node = stack.Pop();
                
                foreach (var child in node.Children)
                {
                    child.Parent = node;
                    stack.Push(child);
                }

                if (node == Lhs)
                    found.Add(node);
            }

            return found;
        }

        private void Fill(ParseTreeNode found)
        {
            // walk 'Lhs' and 'found' to make contents in 'Lhs' match contents in 'found'
            var stack1 = new Stack<ParseTreeNode>();
            var stack2 = new Stack<ParseTreeNode>();
            stack1.Push(found);
            stack2.Push(Lhs);
            while (stack1.Count > 0)
            {
                var node1 = stack1.Pop();
                var node2 = stack2.Pop();
                
                foreach (var child in node1.Children)
                {
                    stack1.Push(child);    
                }

                if (node2.IsSymbolic)
                {
                    node2.Symbol = "?";
                }
                
                if (node1.Symbol != node2.Symbol)
                {
                    node2.Parent = node1.Parent;
                    node2.Children = node1.Children;
                    node2.Symbol = node1.Symbol;
                }
                
                foreach (var child in node2.Children)
                {
                    stack2.Push(child); 
                }

                node2.Token = node1.Token;
                node2.Identifier = node1.Identifier;
            }
        }
        
        public bool Apply(ParseTreeNode root)
        {            
            var found = Find(root);
            if (found.Count == 0) return false;

            foreach (var node in found)
            {
                if (Rhs == null)
                {
                    var r = node.Parent;
                    node.Parent.Children.Remove(node);
                    node.Parent = null;
                    if (Callback != null)
                    {
                        return Callback(r);
                    }
                }
                else if(node.Parent != null)
                {
                    Fill(node);

                    int index = node.Parent.Children.IndexOf(node);
                    node.Parent.Children.Remove(node);
                    node.Children.Remove(Rhs);
                    var r = new ParseTreeNode(Rhs);
                    node.Parent.Children.Insert(index, r);
                    r.Parent = node.Parent;
                    foreach (var child in node.Parent.Children)
                    {
                        child.IsSymbolic = false;
                    }
                    node.Parent = null;
                    
                    if (Callback != null)
                    {
                        return Callback(r);
                    }
                }
                else
                {
                    Fill(node);
                    
                    if (Callback != null)
                    {
                        return Callback(node);
                    }
                }
            }
            
            return true;
        }
    }
    
    public class TreeOptimizer
    {
        private List<TreeOptimizerRule> Rules;

        public TreeOptimizer()
        {
            Rules = new List<TreeOptimizerRule>();
        }

        public void AddRule(int priority, ParseTreeNode find, ParseTreeNode replace, TreeOptimizerRule.RuleCallback cb=null)
        {
            Rules.Add(new TreeOptimizerRule(priority, find, replace, cb));
        }

        public ParseTreeNode Optimize(ParseTreeNode root)
        {
            Rules.Sort();
            Rules.Reverse();

            bool working = true;
            while(working)
            {
                working = false;
                foreach (var rule in Rules)
                {
                    if (rule.Apply(root))
                    {
                        working = true;
                        break;
                    }
                }
            }
            
            return root;
        }
    }
}