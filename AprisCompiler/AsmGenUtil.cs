using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AprisCompiler
{
    public enum BasicType
    {
        Void, 
        U8, U16, U32, U64, 
        I8, I16, I32, I64, 
        F32, F64,
        Bit, Bool,
        Str, String, 
        Array, List, 
        Struct, Class, 
        Enum, Interface,
        Tuple, Function,
        Custom, Unknown,
    }
    
    // \w+ *-> *(( *\w+ *)+|\|)+

    public enum AdvancedType
    {
        
    }

    public class MemoryInfo
    {
        public bool UseMemory = false;
        public bool IsVolatile = false;
        public ulong Location = 0;
        
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            
            if(obj is MemoryInfo v)
            {
                return UseMemory == v.UseMemory && IsVolatile == v.IsVolatile && Location == v.Location;
            }

            return false;
        }
        
        public static bool operator ==(MemoryInfo o1, MemoryInfo o2)
        {
            return Equals(o1, o2);
        }
    
        public static bool operator !=(MemoryInfo o1, MemoryInfo o2)
        {
            return !(o1 == o2);
        }

        public override int GetHashCode()
        {
            int hash = 23498756;
            hash ^= UseMemory.GetHashCode();
            hash ^= IsVolatile.GetHashCode();
            hash ^= Location.GetHashCode();
            return hash;
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append("MemoryInfo<UseMemory: ");
            builder.Append(UseMemory);
            builder.Append(", IsVolatile: ");
            builder.Append(IsVolatile);
            builder.Append(", Location: ");
            builder.Append(Location);
            builder.Append(">");
            return builder.ToString();
        }
    }
    
    public class VariableType
    {
        public int Size = 0;
        public BasicType BaseType = BasicType.Unknown;
        public string TypeName = "Unknown";
        public List<VariableType> Generics = new List<VariableType>();
        public MemoryInfo MemoryInfo = new MemoryInfo();

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            
            if(obj is VariableType v)
            {
                return BaseType == v.BaseType && TypeName == v.TypeName && Generics.SequenceEqual(v.Generics) && MemoryInfo == v.MemoryInfo;
            }

            return false;
        }
        
        public static bool operator ==(VariableType o1, VariableType o2)
        {
            return Equals(o1, o2);
        }
    
        public static bool operator !=(VariableType o1, VariableType o2)
        {
            return !(o1 == o2);
        }

        public override int GetHashCode()
        {
            int hash = 67348956;
            hash ^= BaseType.GetHashCode();
            hash ^= TypeName.GetHashCode();
            foreach (var type in Generics)
            {
                hash ^= type.GetHashCode();
            }
            hash ^= MemoryInfo.GetHashCode();
            return hash;
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append("VariableType<BaseType: ");
            builder.Append(AsmGenUtil.GetBasicTypeAsString(BaseType));
            builder.Append(", TypeName: ");
            builder.Append(TypeName);
            if (MemoryInfo.UseMemory)
            {
                builder.Append(", MemoryInfo: ");
                builder.Append(MemoryInfo);
            }
            if (Generics.Count != 0)
            {
                builder.Append(", Generics: <");
                bool r = false;
                foreach (var variableType in Generics)
                {
                    builder.Append(variableType);
                    builder.Append(", ");
                    r = true;
                }

                if (r)
                {
                    builder.Remove(builder.Length - 2, 2);
                }

                builder.Append(">");
            }
            builder.Append(">");
            return builder.ToString();
        }
    }
    
    public class FunctionType : VariableType
    {
        public VariableType ReturnType = new VariableType();
        public List<Tuple<string, string, VariableType>> Arguments = new List<Tuple<string, string, VariableType>>();
        
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            
            if(obj is FunctionType v)
            {
                return BaseType == v.BaseType && TypeName == v.TypeName && Generics.SequenceEqual(v.Generics) && MemoryInfo == v.MemoryInfo
                    && ReturnType == v.ReturnType && Arguments.SequenceEqual(v.Arguments);
            }

            return false;
        }
        
        public static bool operator ==(FunctionType o1, FunctionType o2)
        {
            return Equals(o1, o2);
        }
    
        public static bool operator !=(FunctionType o1, FunctionType o2)
        {
            return !(o1 == o2);
        }

        public override int GetHashCode()
        {
            int hash = 67348956;
            hash ^= BaseType.GetHashCode();
            hash ^= TypeName.GetHashCode();
            foreach (var type in Generics)
            {
                hash ^= type.GetHashCode();
            }
            hash ^= MemoryInfo.GetHashCode();
            if(ReturnType != null)
            {
                hash ^= ReturnType.GetHashCode();
            }
            foreach (var v in Arguments)
            {
                hash ^= v.GetHashCode();
            }
            return hash;
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append("FunctionType<BaseType: ");
            builder.Append(AsmGenUtil.GetBasicTypeAsString(BaseType));
            builder.Append(", TypeName: ");
            builder.Append(TypeName);
            if (MemoryInfo.UseMemory)
            {
                builder.Append(", MemoryInfo: ");
                builder.Append(MemoryInfo);
            }
            if (Generics.Count != 0)
            {
                builder.Append(", Generics: <");
                bool r = false;
                foreach (var variableType in Generics)
                {
                    builder.Append(variableType);
                    builder.Append(", ");
                    r = true;
                }

                if (r)
                {
                    builder.Remove(builder.Length - 2, 2);
                }

                builder.Append(">");
            }
            builder.Append(", ReturnType: ");
            builder.Append(ReturnType);
            if (Arguments.Count != 0)
            {
                builder.Append(", Arguments: <");
                bool r = false;
                foreach (var item in Arguments)
                {
                    builder.Append(item);
                    builder.Append(", ");
                    r = true;
                }

                if (r)
                {
                    builder.Remove(builder.Length - 2, 2);
                }

                builder.Append(">");
            }
            builder.Append(">");
            return builder.ToString();
        }
    }

    public class Variable
    {
        public string Identifier = "";
        public VariableType Type = new VariableType();
        public bool IsConstant = false;

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            
            if(obj is Variable v)
            {
                return Identifier == v.Identifier && Type == v.Type && IsConstant == v.IsConstant;
            }

            return false;
        }
        
        public static bool operator ==(Variable o1, Variable o2)
        {
            return Equals(o1, o2);
        }
    
        public static bool operator !=(Variable o1, Variable o2)
        {
            return !(o1 == o2);
        }

        public override int GetHashCode()
        {
            int hash = 323897645;
            hash ^= Identifier.GetHashCode();
            hash ^= Type.GetHashCode();
            hash ^= IsConstant.GetHashCode();
            return hash;
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append("Variable<Identifier: ");
            builder.Append(Identifier);
            builder.Append(", Type: ");
            builder.Append(Type);
            builder.Append(", IsConstant: ");
            builder.Append(IsConstant);
            builder.Append(">");
            return builder.ToString();
        }
    }

    public class Function
    {
        public string Identifier = "";
        public FunctionType Type = new FunctionType();
        
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            
            if(obj is Function v)
            {
                return Identifier == v.Identifier && Type == v.Type;
            }

            return false;
        }
        
        public static bool operator ==(Function o1, Function o2)
        {
            return Equals(o1, o2);
        }
    
        public static bool operator !=(Function o1, Function o2)
        {
            return !(o1 == o2);
        }

        public override int GetHashCode()
        {
            int hash = 994857637;
            hash ^= Identifier.GetHashCode();
            hash ^= Type.GetHashCode();
            return hash;
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append("Function<Identifier: ");
            builder.Append(Identifier);
            builder.Append(", Type: ");
            builder.Append(Type);
            builder.Append(">");
            return builder.ToString();
        }
    }

    public class Scope
    {
        public bool IsGlobalScope = false;
        public int ScopeByteSize = 0;
        public Dictionary<string, Variable> Variables = new Dictionary<string, Variable>();
        public Dictionary<string, Function> Functions = new Dictionary<string, Function>();

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            
            if(obj is Scope v)
            {
                return ScopeByteSize == v.ScopeByteSize && Variables.SequenceEqual(v.Variables) && Functions.SequenceEqual(v.Functions);
            }

            return false;
        }
        
        public static bool operator ==(Scope o1, Scope o2)
        {
            return Equals(o1, o2);
        }
    
        public static bool operator !=(Scope o1, Scope o2)
        {
            return !(o1 == o2);
        }

        public override int GetHashCode()
        {
            int hash = 498342856;
            hash ^= ScopeByteSize.GetHashCode();
            foreach ((string key, Variable value) in Variables)
            {
                hash ^= key.GetHashCode();
                hash ^= value.GetHashCode();
            }
            foreach ((string key, Function value) in Functions)
            {
                hash ^= key.GetHashCode();
                hash ^= value.GetHashCode();
            }
            return hash;
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append("Scope{\n\tScopeByteSize: ");
            builder.Append(ScopeByteSize);
            builder.Append("\n\tVariables: ");
            
            foreach ((string key, Variable value) in Variables)
            {
                builder.Append("\t\t");
                builder.Append(key);
                builder.Append(" -> ");
                builder.Append(value);
                builder.Append("\n");
            }
            
            builder.Append("\n\tFunctions: ");
            
            foreach ((string key, Function value) in Functions)
            {
                builder.Append("\t\t");
                builder.Append(key);
                builder.Append(" -> ");
                builder.Append(value);
                builder.Append("\n");
            }
            
            builder.Append("}");
            return builder.ToString();
        }
    }
    
    public static class AsmGenUtil
    {
        public static readonly VariableType VoidType = new VariableType { BaseType = BasicType.Void, Generics = new List<VariableType>(), TypeName = "Void" };
        public static readonly VariableType U8Type = new VariableType { BaseType = BasicType.U8, Generics = new List<VariableType>(), TypeName = "Bool" };
        public static readonly VariableType U16Type = new VariableType { BaseType = BasicType.U16, Generics = new List<VariableType>(), TypeName = "Bool" };
        public static readonly VariableType U32Type = new VariableType { BaseType = BasicType.U32, Generics = new List<VariableType>(), TypeName = "Bool" };
        public static readonly VariableType U64Type = new VariableType { BaseType = BasicType.U64, Generics = new List<VariableType>(), TypeName = "Bool" };
        public static readonly VariableType I8Type = new VariableType { BaseType = BasicType.I8, Generics = new List<VariableType>(), TypeName = "Bool" };
        public static readonly VariableType I16Type = new VariableType { BaseType = BasicType.I16, Generics = new List<VariableType>(), TypeName = "Bool" };
        public static readonly VariableType I32Type = new VariableType { BaseType = BasicType.I32, Generics = new List<VariableType>(), TypeName = "Bool" };
        public static readonly VariableType I64Type = new VariableType { BaseType = BasicType.I64, Generics = new List<VariableType>(), TypeName = "Bool" };
        public static readonly VariableType F32Type = new VariableType { BaseType = BasicType.F32, Generics = new List<VariableType>(), TypeName = "Bool" };
        public static readonly VariableType F64Type = new VariableType { BaseType = BasicType.F64, Generics = new List<VariableType>(), TypeName = "Bool" };
        public static readonly VariableType BitType = new VariableType { BaseType = BasicType.Bit, Generics = new List<VariableType>(), TypeName = "Bool" };
        public static readonly VariableType BoolType = new VariableType { BaseType = BasicType.Bool, Generics = new List<VariableType>(), TypeName = "Bool" };
        //public static readonly VariableType StrType = new VariableType { BaseType = BasicType.Str, Generics = new List<VariableType>(), TypeName = "Bool" };
        //public static readonly VariableType StringType = new VariableType { BaseType = BasicType.String, Generics = new List<VariableType>(), TypeName = "Bool" };
        //public static readonly VariableType ArrayType = new VariableType { BaseType = BasicType.Array, Generics = new List<VariableType>(), TypeName = "Bool" };
        //public static readonly VariableType ListType = new VariableType { BaseType = BasicType.List, Generics = new List<VariableType>(), TypeName = "Bool" };
        //public static readonly VariableType StructType = new VariableType { BaseType = BasicType.Struct, Generics = new List<VariableType>(), TypeName = "Bool" };
        //public static readonly VariableType ClassType = new VariableType { BaseType = BasicType.Class, Generics = new List<VariableType>(), TypeName = "Bool" };
        //public static readonly VariableType EnumType = new VariableType { BaseType = BasicType.Enum, Generics = new List<VariableType>(), TypeName = "Bool" };
        //public static readonly VariableType InterfaceType = new VariableType { BaseType = BasicType.Interface, Generics = new List<VariableType>(), TypeName = "Bool" };
        //public static readonly VariableType TupleType = new VariableType { BaseType = BasicType.Tuple, Generics = new List<VariableType>(), TypeName = "Bool" };
        //public static readonly VariableType FunctionType = new VariableType { BaseType = BasicType.Function, Generics = new List<VariableType>(), TypeName = "Bool" };
        //public static readonly VariableType CustomType = new VariableType { BaseType = BasicType.Custom, Generics = new List<VariableType>(), TypeName = "Bool" };
        //public static readonly VariableType UnknownType = new VariableType { BaseType = BasicType.Unknown, Generics = new List<VariableType>(), TypeName = "Bool" };
        
        public static string GetBasicTypeAsString(BasicType t)
        {
            return Enum.GetName(typeof(BasicType), t);
        }

        public static BasicType GetBasicTypeFromString(string s)
        {
            switch (s.ToLower().Trim())
            {
                case "void":      return BasicType.Void;
                case "u8":        return BasicType.U8;
                case "u16":       return BasicType.U16;
                case "u32":       return BasicType.U32;
                case "u64":       return BasicType.U64;
                case "i8":        return BasicType.I8;
                case "i16":       return BasicType.I16;
                case "i32":       return BasicType.I32;
                case "i64":       return BasicType.I64;
                case "f32":       return BasicType.F32;
                case "f64":       return BasicType.F64;
                case "bit":       return BasicType.Bit;
                case "bool":      return BasicType.Bool;
                case "str":       return BasicType.Str;
                case "string":    return BasicType.String;
                case "array":     return BasicType.Array;
                case "list":      return BasicType.List;
                case "struct":    return BasicType.Struct;
                case "class":     return BasicType.Class;
                case "enum":      return BasicType.Enum;
                case "interface": return BasicType.Interface;
                case "tuple":     return BasicType.Tuple;
                case "function":  return BasicType.Function;
                case "custom":    return BasicType.Custom;
                case "unknown":
                default:          return BasicType.Unknown;
            }
        }

        public static int GetBasicTypeByteSize(BasicType t)
        {
            switch (t)
            {
                case BasicType.Void: return 0;
                case BasicType.Bool:
                case BasicType.U8:
                case BasicType.I8: return 1;
                case BasicType.U16:
                case BasicType.I16: return 2;
                case BasicType.F32:
                case BasicType.U32:
                case BasicType.I32: return 4;
                case BasicType.F64:
                case BasicType.U64: 
                case BasicType.I64: return 8;
                default: throw new Exception("No defined size exists for Basic Type: " + GetBasicTypeAsString(t));
            }
        }
        
        public static bool HaveBasicTypeByteSize(BasicType t)
        {
            switch (t)
            {
                case BasicType.Void:
                case BasicType.Bool:
                case BasicType.U8:
                case BasicType.I8:
                case BasicType.U16:
                case BasicType.I16:
                case BasicType.F32:
                case BasicType.U32:
                case BasicType.I32:
                case BasicType.F64:
                case BasicType.U64: 
                case BasicType.I64: return true;
                default: return false;
            }
        }

        public static bool IsFloatingType(BasicType t)
        {
            return t == BasicType.F32 || t == BasicType.F64;
        }

        public static bool IsIntegerType(BasicType t)
        {
            return IsSignedIntegerType(t) || IsUnsignedIntegerType(t);
        }
        
        public static bool IsSignedIntegerType(BasicType t)
        {
            return t == BasicType.I8 || t == BasicType.I16 || t == BasicType.I32 || t == BasicType.I64;
        }
        
        public static bool IsUnsignedIntegerType(BasicType t)
        {
            return t == BasicType.U8 || t == BasicType.U16 || t == BasicType.U32 || t == BasicType.U64;
        }
        
        public static string LineArrayToString(IEnumerable<string> lst)
        {
            var builder = new StringBuilder();
            foreach (string s in lst)
            {
                builder.Append(s);
                builder.Append("\n");
            }
            return builder.ToString();
        }
        
        private static readonly string[] WinRegs = { "rcx", "rdx", "r8", "r9" };
        private static readonly string[] LinRegs = { "rdi", "rsi", "rdx", "rcx" };
        public static string GetArgRegister(int num)
        {
            switch (ExeTools.OperatingSystem)
            {
                case ExeTools.Os.Win: return WinRegs[num];
                case ExeTools.Os.Linux:
                case ExeTools.Os.Mac: return LinRegs[num];
                default: throw new Exception("Unknown OS!");
            }
        }

        public static bool CanCoerceTypes(VariableType from, VariableType to, bool cast=false)
        {
            // A type is able to be coerced to the same type
            if (from == to)
            {
                return true;
            }
            
            // Integers become bigger to match size of operand & Signed integers will be reinterpreted to Unsigned Integers
            if (IsIntegerType(from.BaseType) && (IsFloatingType(to.BaseType) || IsIntegerType(to.BaseType)))
            {
                // if 'from' is <= in size than 'to' or casting it then conversion is possible

                if ((IsUnsignedIntegerType(from.BaseType) && IsSignedIntegerType(to.BaseType)) ||
                    IsUnsignedIntegerType(to.BaseType) && IsSignedIntegerType(from.BaseType))
                {
                    Console.WriteLine("Warning: Signed/Unsigned Mismatch.");
                }
                
                return GetBasicTypeByteSize(from.BaseType) <= GetBasicTypeByteSize(to.BaseType) || cast;
            }
            
            // Floats become bigger to match size of operand
            if (IsFloatingType(from.BaseType) && IsFloatingType(to.BaseType))
            {
                return GetBasicTypeByteSize(from.BaseType) <= GetBasicTypeByteSize(to.BaseType) || cast;
            }
            
            // Integers, Floats, and Bit can be reinterpreted to booleans
            if ((IsIntegerType(from.BaseType) || IsFloatingType(from.BaseType) || from.BaseType == BasicType.Bit) && to.BaseType == BasicType.Bool)
            {
                return true;
            }

            // Bool and Bit can be reinterpreted to Integers & Floats
            if ((from.BaseType == BasicType.Bit || from.BaseType == BasicType.Bool) && (IsIntegerType(to.BaseType) || IsFloatingType(to.BaseType)))
            {
                return true;
            }
            
            // TODO: Implement more?
            // what about classes?

            return false;
        }
        
        public static VariableType CoerceType(VariableType toCoerce, VariableType toCoerceTo)
        {
            if (!CanCoerceTypes(toCoerce, toCoerceTo))
            {
                throw new Exception("Cannot Coerce " + toCoerce.TypeName + " into " + toCoerceTo.TypeName + "!");
            }
            
            if (toCoerce == toCoerceTo)
            {
                return toCoerce;
            }
            
            // TODO: Something else here? This feels incomplete.

            return toCoerceTo;
        }
    }
}

/*
 * Basic Type Coercion Rules:
 *     ~ A type is able to be coerced to the same type:
 *         - u8, u8 = u8
 *         - i64, i64 = i64
 *     ~ Integers become bigger to match size of operand:
 *         - u8, u16 = u16
 *         - i16, i64 = i64
 *     ~ Floats become bigger to match size of operand:
 *         - f32, f64 = f64
 *     ~ Integers become smallest float type (while still following the above rules):
 *         - u8, f32 = f32
 *         - u8, f64 = f64
 *         - i16, f32 = f32
 *     ~ Signed integers will be reinterpreted to Unsigned Integers (and will emit a warning unless casted):
 *         - i8, u8 = u8
 *         - i8, u32 = u32
 *     ~ Bool and Bit can be reinterpreted to Integers & Floats:
 *         - bool, u8 = u8
 *         - bool, i32 = i32
 *         - bit, i8 = i8
 *         - bool, f64 = f64
 *     ~ Integers and Bit can be reinterpreted to booleans:
 *         - u8, bool = bool
 *         - i64, bool = bool
 *         - bit, bool = bool
 *     ~ Bigger types can sometimes go to lower ones with a type cast:
 *         - u64, u8, cast = u8
 */
