using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AprisCompiler
{
    public class Utility
    {
        private static string HtmlEscape(string s)
        {
            s = s.Replace("&", "&amp;");
            s = s.Replace("<", "&lt;");
            s = s.Replace(">", "&gt;");
            s = s.Replace("\"", "&quot;");
            s = s.Replace("]", " ]");
            return s;
        }
        
        public static void SaveDfaToDot(string filePath, DfaState startState)
        {
            StreamWriter writer = new StreamWriter(filePath, false);
            
            writer.WriteLine("digraph G {");

            HashSet<int> seen = new HashSet<int>();
            Stack<DfaState> todo = new Stack<DfaState>();
            todo.Push(startState);
            seen.Add(startState.Identifier);
            
            while (todo.Count > 0)
            {
                DfaState state = todo.Pop();
                
                writer.Write("\t");
                writer.Write(state.Identifier);
                writer.Write(" [shape=");
                writer.Write(state.Identifier == 0 ? "box,style=diagonals,label=<" : "box,label=<");

                StringBuilder builder = new StringBuilder();
                writer.Write(state.Identifier);
                builder.Append("<br/>");
                foreach (Lr0Item lr0Item in state.ItemSet)
                {
                    builder.Append(HtmlEscape(lr0Item.Lhs));
                    builder.Append(" &rarr; ");
                    for (int i = 0; i < lr0Item.Rhs.Count; i++)
                    {
                        if (i == lr0Item.Dpos)
                        {
                            builder.Append("&bull;");
                        }
                        builder.Append(HtmlEscape(lr0Item.Rhs[i]));
                        builder.Append(" ");
                    }
                    builder.Remove(builder.Length - 1, 1);
                    if (lr0Item.DposAtEnd())
                    {
                        builder.Append("&bull;");
                    }
                    builder.Append("<br/>");
                }
                builder.Remove(builder.Length - 5, 5);
                writer.Write(builder.ToString());
                
                writer.WriteLine(">];");
                
                foreach ((string transition, DfaState to) in state.Transitions)
                {
                    if (!seen.Contains(to.Identifier))
                    {
                        seen.Add(to.Identifier);
                        todo.Push(to);
                    }
                    
                    writer.Write("\t");
                    writer.Write(state.Identifier);
                    writer.Write(" -> ");
                    writer.Write(to.Identifier);
                    writer.Write(" [label=<");
                    writer.Write(HtmlEscape(transition));
                    writer.WriteLine(">];");
                }
            }

            writer.WriteLine("}");
            writer.Close();
        }
        
        public static void SaveParseTreeToDot(string filePath, ParseTreeNode root)
        {
            StreamWriter writer = new StreamWriter(filePath, false);
            if (root == null)
            {
                writer.Close();
                return;
            }
            
            writer.WriteLine("digraph G {");

            HashSet<int> seen = new HashSet<int>();
            Stack<ParseTreeNode> todo = new Stack<ParseTreeNode>();
            todo.Push(root);
            seen.Add(root.Identifier);
            
            while (todo.Count > 0)
            {
                ParseTreeNode node = todo.Pop();
                
                writer.Write("\t");
                writer.Write(node.Identifier);
                writer.Write(" [shape=");
                writer.Write(node.Identifier == 0 ? "box,style=diagonals,label=<" : "box,label=<");
                writer.Write(HtmlEscape(node.Symbol));
                writer.Write("<br/>");
                writer.Write(HtmlEscape(node.Token));
                writer.WriteLine(">];");
                
                foreach (ParseTreeNode child in node.Children)
                {
                    if (!seen.Contains(child.Identifier))
                    {
                        seen.Add(child.Identifier);
                        todo.Push(child);
                    }
                    
                    writer.Write("\t");
                    writer.Write(node.Identifier);
                    writer.Write(" -> ");
                    writer.WriteLine(child.Identifier);
                }
            }

            writer.WriteLine("}");
            writer.Close();
        }

        public static void SaveLl1ParseTableToHtml(string filePath, Dictionary<string, Dictionary<string, Production>> table, HashSet<Terminal> terminals, HashSet<NonTerminal> nonTerminals)
        {
            StreamWriter writer = new StreamWriter(filePath, false);

            terminals = new HashSet<Terminal>(terminals) { new Terminal("$", "") };

            // Write out the starter boiler plate
            writer.WriteLine("<!DOCTYPE html>");
            writer.WriteLine("<html>");
            writer.WriteLine("\t<head>");
            writer.WriteLine("\t\t<style>");
            writer.WriteLine("\t\t\ttable, th, td {");
            writer.WriteLine("\t\t\t\tborder: 1px solid black;");
            writer.WriteLine("\t\t\t\tborder-collapse: collapse;");
            writer.WriteLine("\t\t\t}");
            writer.WriteLine("\t\t\tth, td {");
            writer.WriteLine("\t\t\t\tpadding: 15px;");
            writer.WriteLine("\t\t\t\twhite-space: nowrap;");
            writer.WriteLine("\t\t\t}");
            writer.WriteLine("\t\t\ttd {");
            writer.WriteLine("\t\t\t\ttext-align: center;");
            writer.WriteLine("\t\t\t}");
            writer.WriteLine("\t\t\ttable tr:nth-child(even) {");
            writer.WriteLine("\t\t\t\tbackground-color: #eee;");
            writer.WriteLine("\t\t\t}");
            writer.WriteLine("\t\t\ttable tr:nth-child(odd) {");
            writer.WriteLine("\t\t\t\tbackground-color: #fff;");
            writer.WriteLine("\t\t\t}");
            writer.WriteLine("\t\t\ttable th {");
            writer.WriteLine("\t\t\t\tbackground-color: black;");
            writer.WriteLine("\t\t\t\tcolor: white;");
            writer.WriteLine("\t\t\t\tborder: 1px solid white;");
            writer.WriteLine("\t\t\t}");
            writer.WriteLine("\t\t\ttr td:first-child {");
            writer.WriteLine("\t\t\t\tbackground-color: black;");
            writer.WriteLine("\t\t\t\tcolor: white;");
            writer.WriteLine("\t\t\t\tborder: 1px solid white;");
            writer.WriteLine("\t\t\t\ttext-align: left;");
            writer.WriteLine("\t\t\t}");
            writer.WriteLine("\t\t</style>");
            writer.WriteLine("\t</head>");
            writer.WriteLine("\t<body>");
            writer.WriteLine("\t\t<table>");
            
            // Write out the table header (Terminals)
            writer.WriteLine("\t\t\t<tr>");
            writer.WriteLine("\t\t\t\t<th>LL(1) Parse Table</th>");
            foreach (Terminal terminal in terminals)
            {
                writer.Write("\t\t\t\t<th>");
                writer.Write(HtmlEscape(terminal.Lhs));
                writer.WriteLine("</th>");
            }
            writer.WriteLine("\t\t\t</tr>");
            
            // Write out the rest of it (nt,t)
            foreach (NonTerminal nonTerminal in nonTerminals)
            {
                writer.WriteLine("\t\t\t<tr>");
                writer.Write("\t\t\t\t<td>");
                writer.Write(HtmlEscape(nonTerminal.Lhs));
                writer.WriteLine("</td>");
                foreach (Terminal terminal in terminals)
                {
                    writer.Write("\t\t\t\t<td>");
                    if (table.ContainsKey(nonTerminal.Lhs) && table[nonTerminal.Lhs].ContainsKey(terminal.Lhs))
                    {
                        string p = HtmlEscape(table[nonTerminal.Lhs][terminal.Lhs].ToString());
                        p = p.Replace("-&gt;", "&rarr;");
                        p = p.Replace("lambda", "&lambda;");
                        writer.Write(p);
                    }
                    writer.WriteLine("</td>");
                }            
                
                writer.WriteLine("</tr>");
            }
            
            // Write out the final boiler plate
            writer.WriteLine("\t\t</table>");
            writer.WriteLine("\t</body>");
            writer.WriteLine("</html>");
            writer.Close();
        }

        public static void SaveSlrParseTableToHtml(string filePath, List<Dictionary<string, LrRule>> slrTable, HashSet<Terminal> terminals, HashSet<NonTerminal> nonTerminals)
        {
            StreamWriter writer = new StreamWriter(filePath, false);

            terminals = new HashSet<Terminal>(terminals) { new Terminal("$", "") };

            // Write out the starter boiler plate
            writer.WriteLine("<!DOCTYPE html>");
            writer.WriteLine("<html>");
            writer.WriteLine("\t<head>");
            writer.WriteLine("\t\t<style>");
            writer.WriteLine("\t\t\ttable, th, td {");
            writer.WriteLine("\t\t\t\tborder: 1px solid black;");
            writer.WriteLine("\t\t\t\tborder-collapse: collapse;");
            writer.WriteLine("\t\t\t}");
            writer.WriteLine("\t\t\tth, td {");
            writer.WriteLine("\t\t\t\tpadding: 15px;");
            writer.WriteLine("\t\t\t\twhite-space: nowrap;");
            writer.WriteLine("\t\t\t}");
            writer.WriteLine("\t\t\ttd {");
            writer.WriteLine("\t\t\t\ttext-align: center;");
            writer.WriteLine("\t\t\t}");
            writer.WriteLine("\t\t\ttable tr:nth-child(even) {");
            writer.WriteLine("\t\t\t\tbackground-color: #eee;");
            writer.WriteLine("\t\t\t}");
            writer.WriteLine("\t\t\ttable tr:nth-child(odd) {");
            writer.WriteLine("\t\t\t\tbackground-color: #fff;");
            writer.WriteLine("\t\t\t}");
            writer.WriteLine("\t\t\ttable th {");
            writer.WriteLine("\t\t\t\tbackground-color: black;");
            writer.WriteLine("\t\t\t\tcolor: white;");
            writer.WriteLine("\t\t\t\tborder: 1px solid white;");
            writer.WriteLine("\t\t\t}");
            writer.WriteLine("\t\t\ttr td:first-child {");
            writer.WriteLine("\t\t\t\tbackground-color: black;");
            writer.WriteLine("\t\t\t\tcolor: white;");
            writer.WriteLine("\t\t\t\tborder: 1px solid white;");
            writer.WriteLine("\t\t\t\ttext-align: left;");
            writer.WriteLine("\t\t\t}");
            writer.WriteLine("\t\t</style>");
            writer.WriteLine("\t</head>");
            writer.WriteLine("\t<body>");
            writer.WriteLine("\t\t<table>");
            
            // Write out the table header (Terminals)
            writer.WriteLine("\t\t\t<tr>");
            writer.WriteLine("\t\t\t\t<th>SLR(1) Parse Table</th>");
            foreach (Terminal terminal in terminals)
            {
                writer.Write("\t\t\t\t<th>");
                writer.Write(HtmlEscape(terminal.Lhs));
                writer.WriteLine("</th>");
            }
            foreach (NonTerminal nonterminal in nonTerminals)
            {
                writer.Write("\t\t\t\t<th>");
                writer.Write(HtmlEscape(nonterminal.Lhs));
                writer.WriteLine("</th>");
            }
            writer.WriteLine("\t\t\t</tr>");
            
            for(int i = 0; i < slrTable.Count; i++)
            {
                writer.WriteLine("\t\t\t<tr>");
                writer.Write("\t\t\t\t<td>");
                writer.Write(HtmlEscape(i.ToString()));
                writer.WriteLine("</td>");
                
                foreach (Terminal terminal in terminals)
                {
                    writer.Write("\t\t\t\t<td>");
                    if (slrTable[i].ContainsKey(terminal.Lhs))
                    {
                        string p = HtmlEscape(slrTable[i][terminal.Lhs].ToFriendlyTableString());
                        p = p.Replace("-&gt;", "&rarr;");
                        p = p.Replace("lambda", "&lambda;");
                        writer.Write(p);
                    }
                    writer.WriteLine("</td>");
                }
                
                foreach (NonTerminal nonTerminal in nonTerminals)
                {
                    writer.Write("\t\t\t\t<td>");
                    if (slrTable[i].ContainsKey(nonTerminal.Lhs))
                    {
                        string p = HtmlEscape(slrTable[i][nonTerminal.Lhs].ToFriendlyTableString());
                        p = p.Replace("-&gt;", "&rarr;");
                        p = p.Replace("lambda", "&lambda;");
                        writer.Write(p);
                    }
                    writer.WriteLine("</td>");
                }
                writer.WriteLine("\t\t\t</tr>");
            }
            
            // Write out the final boiler plate
            writer.WriteLine("\t\t</table>");
            writer.WriteLine("\t</body>");
            writer.WriteLine("</html>");
            writer.Close();
        }
        
        public static void SaveGlrParseTableToHtml(string filePath, List<Dictionary<string, List<LrRule>>> glrTable, HashSet<Terminal> terminals, HashSet<NonTerminal> nonTerminals)
        {
            StreamWriter writer = new StreamWriter(filePath, false);

            terminals = new HashSet<Terminal>(terminals) { new Terminal("$", "") };

            // Write out the starter boiler plate
            writer.WriteLine("<!DOCTYPE html>");
            writer.WriteLine("<html>");
            writer.WriteLine("\t<head>");
            writer.WriteLine("\t\t<style>");
            writer.WriteLine("\t\t\ttable, th, td {");
            writer.WriteLine("\t\t\t\tborder: 1px solid black;");
            writer.WriteLine("\t\t\t\tborder-collapse: collapse;");
            writer.WriteLine("\t\t\t}");
            writer.WriteLine("\t\t\tth, td {");
            writer.WriteLine("\t\t\t\tpadding: 15px;");
            writer.WriteLine("\t\t\t\twhite-space: nowrap;");
            writer.WriteLine("\t\t\t}");
            writer.WriteLine("\t\t\ttd {");
            writer.WriteLine("\t\t\t\ttext-align: center;");
            writer.WriteLine("\t\t\t}");
            writer.WriteLine("\t\t\ttable tr:nth-child(even) {");
            writer.WriteLine("\t\t\t\tbackground-color: #eee;");
            writer.WriteLine("\t\t\t}");
            writer.WriteLine("\t\t\ttable tr:nth-child(odd) {");
            writer.WriteLine("\t\t\t\tbackground-color: #fff;");
            writer.WriteLine("\t\t\t}");
            writer.WriteLine("\t\t\ttable th {");
            writer.WriteLine("\t\t\t\tbackground-color: black;");
            writer.WriteLine("\t\t\t\tcolor: white;");
            writer.WriteLine("\t\t\t\tborder: 1px solid white;");
            writer.WriteLine("\t\t\t}");
            writer.WriteLine("\t\t\ttr td:first-child {");
            writer.WriteLine("\t\t\t\tbackground-color: black;");
            writer.WriteLine("\t\t\t\tcolor: white;");
            writer.WriteLine("\t\t\t\tborder: 1px solid white;");
            writer.WriteLine("\t\t\t\ttext-align: left;");
            writer.WriteLine("\t\t\t}");
            writer.WriteLine("\t\t</style>");
            writer.WriteLine("\t</head>");
            writer.WriteLine("\t<body>");
            writer.WriteLine("\t\t<table>");
            
            // Write out the table header (Terminals)
            writer.WriteLine("\t\t\t<tr>");
            writer.WriteLine("\t\t\t\t<th>SLR(1) Parse Table</th>");
            foreach (Terminal terminal in terminals)
            {
                writer.Write("\t\t\t\t<th>");
                writer.Write(HtmlEscape(terminal.Lhs));
                writer.WriteLine("</th>");
            }
            foreach (NonTerminal nonterminal in nonTerminals)
            {
                writer.Write("\t\t\t\t<th>");
                writer.Write(HtmlEscape(nonterminal.Lhs));
                writer.WriteLine("</th>");
            }
            writer.WriteLine("\t\t\t</tr>");
            
            for(int i = 0; i < glrTable.Count; i++)
            {
                writer.WriteLine("\t\t\t<tr>");
                writer.Write("\t\t\t\t<td>");
                writer.Write(HtmlEscape(i.ToString()));
                writer.WriteLine("</td>");
                
                foreach (Terminal terminal in terminals)
                {
                    writer.Write("\t\t\t\t<td");
                    if (glrTable[i].ContainsKey(terminal.Lhs))
                    {
                        writer.Write(glrTable[i][terminal.Lhs].Count > 1 ? " style=\"background-color: red; color: white;\">" : ">");
                        foreach (var entry in glrTable[i][terminal.Lhs])
                        {
                            string p = HtmlEscape(entry.ToFriendlyTableString());
                            p = p.Replace("-&gt;", "&rarr;");
                            p = p.Replace("lambda", "&lambda;");
                            writer.Write(p);
                            writer.Write("<br>");
                        }
                    }
                    else
                    {
                        writer.Write(">");
                    }
                    
                    writer.WriteLine("</td>");
                }
                
                foreach (NonTerminal nonTerminal in nonTerminals)
                {
                    writer.Write("\t\t\t\t<td>");
                    if (glrTable[i].ContainsKey(nonTerminal.Lhs))
                    {
                        foreach (var entry in glrTable[i][nonTerminal.Lhs])
                        {
                            string p = HtmlEscape(entry.ToFriendlyTableString());
                            p = p.Replace("-&gt;", "&rarr;");
                            p = p.Replace("lambda", "&lambda;");
                            writer.Write(p);
                            writer.Write("<br>");
                        }
                    }
                    writer.WriteLine("</td>");
                }
                writer.WriteLine("\t\t\t</tr>");
            }
            
            // Write out the final boiler plate
            writer.WriteLine("\t\t</table>");
            writer.WriteLine("\t</body>");
            writer.WriteLine("</html>");
            writer.Close();
        }

        public static void SaveGlrParseTableToFile(string filePath, List<Dictionary<string, List<LrRule>>> glrTable, HashSet<Terminal> terminals, HashSet<NonTerminal> nonTerminals)
        {
            var writer = new BinaryWriter(new FileStream(filePath, FileMode.Open), Encoding.Default);

            /*
             * - Save Table to Space Efficient File
             *         - u16: # of tokens
             *         - u16: # of states
             *
             *         Token:
             *             - str: name
             *             - str: regex
             *
             *         State:
             *             - u16: # of definitions
             *             - u16: token / production #
             *             - u16: # of operations
             *             - u8: operation type
             *             - u16: s/t state OR # to reduce by
             *             - u16: state #
             */
            
            ushort numTokens = (ushort) (terminals.Count + nonTerminals.Count);
            ushort numStates = (ushort) glrTable.Count;
            
            writer.Write(numTokens);
            writer.Write(numStates);
            
            var idLookup = new Dictionary<string, ushort>();
            
            ushort id = 0;
            foreach (var terminal in terminals)
            {
                idLookup.Add(terminal.Lhs, id);
                writer.Write(terminal.Lhs);
                writer.Write(terminal.Rhs.ToString());
                id++;
            }
            
            idLookup.Add("$", id++);
            writer.Write("$");
            
            foreach (var nonTerminal in nonTerminals)
            {
                idLookup.Add(nonTerminal.Lhs, id++);
                writer.Write(nonTerminal.Lhs);
            }

            for (ushort stateNumber = 0; stateNumber < glrTable.Count; stateNumber++)
            {
                writer.Write((ushort) glrTable[stateNumber].Count);
                
                foreach ((string operationName, List<LrRule> value) in glrTable[stateNumber])
                {
                    writer.Write(idLookup[operationName]);
                    writer.Write((ushort) value.Count);
                    foreach (var rule in value)
                    {
                        byte type = (byte) (rule.GetRuleType() == "R" ? 2 : rule.GetRuleType() == "S" ? 0 : 1);
                        writer.Write(type);
                        if (type == 0 || type == 1)
                        {
                            writer.Write((ushort)rule.GetAsTransitionOrShift());
                        }
                        else
                        {
                            (int numPop, string reduceTo) = rule.GetAsReduce();
                            writer.Write((ushort)numPop);
                            writer.Write(reduceTo);
                        }
                    }
                }
            }

            writer.Close();
        }
        
        public static void LoadGlrParseTableFromFile(string filePath, out List<Dictionary<string, List<LrRule>>> glrTable, out HashSet<Terminal> terminals, out HashSet<NonTerminal> nonTerminals)
        {
            var reader = new BinaryReader(new FileStream(filePath, FileMode.Open), Encoding.Default);

            glrTable = new List<Dictionary<string, List<LrRule>>>();
            terminals = new HashSet<Terminal>();
            nonTerminals = new HashSet<NonTerminal>();
            
            /*
             * - Load Table from Space Efficient File
             *         - u16: # of tokens
             *         - u16: # of states
             *
             *         Token:
             *             - str: name
             *             - str: regex
             *
             *         State:
             *             - u16: # of definitions
             *             - u16: token / production #
             *             - u16: # of operations
             *             - u8: operation type
             *             - u16: s/t state OR # to reduce by
             *             - u16: state #
             */
            
            ushort numTokens = reader.ReadUInt16();
            ushort numStates = reader.ReadUInt16();
            
            var rIdLookup = new Dictionary<ushort, string>();

            ushort tokenId = 0;
            for (; tokenId < numTokens; tokenId++)
            {
                string lhs = reader.ReadString();
                if (lhs == "$")
                {
                    break;
                }
                string rhs = reader.ReadString();
                terminals.Add(new Terminal(lhs, rhs));
                rIdLookup.Add(tokenId, lhs);
            }
            rIdLookup.Add(tokenId++, "$");
            for (; tokenId <= numTokens; tokenId++)
            {
                string lhs = reader.ReadString();
                nonTerminals.Add(new NonTerminal(lhs, new List<List<string>>()));
                rIdLookup.Add(tokenId, lhs);
            }

            for (ushort stateId = 0; stateId < numStates; stateId++)
            {
                ushort count = reader.ReadUInt16();
                glrTable.Add(new Dictionary<string, List<LrRule>>());
                
                for (ushort i = 0; i < count; i++)
                {
                    ushort tpNum = reader.ReadUInt16();
                    string key = rIdLookup[tpNum];
                    glrTable[stateId].Add(key, new List<LrRule>());
                    
                    ushort numOps = reader.ReadUInt16();
                    for (ushort j = 0; j < numOps; j++)
                    {
                        byte type = reader.ReadByte();
                        if (type == 0)
                        {
                            ushort sId = reader.ReadUInt16();
                            glrTable[stateId][key].Add(LrRule.ShiftRule(sId));
                        }
                        else if (type == 1)
                        {
                            ushort tId = reader.ReadUInt16();
                            glrTable[stateId][key].Add(LrRule.TransitionRule(tId));
                        }
                        else
                        {
                            ushort numPop = reader.ReadUInt16();
                            string reduceTo = reader.ReadString();
                            glrTable[stateId][key].Add(LrRule.ReduceRule(numPop, reduceTo));
                        }
                    }
                }
            }

            reader.Close();
        }
    }
}
