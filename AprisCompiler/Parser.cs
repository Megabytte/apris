using System;
using System.Collections.Generic;
using System.Linq;

namespace AprisCompiler
{
    public class StackAsList
    {
        public class Node
        {
            public readonly int Value;
            public readonly Node Next;
            public readonly Tuple<Node, LrRule> How;
        
            public Node(int val, Node nxt, Tuple<Node, LrRule> how)
            {
                Value = val;
                Next = nxt;
                How = how;
            }
        }
        
        private Node _top;
        
        public StackAsList()
        {
            _top = null;
        }

        public void Push(int stateNumber, Tuple<Node, LrRule> how)
        {
            _top = new Node(stateNumber, _top, how);
        }

        public void Pop(int num = 1)
        {
            for (int i = 0; i < num; i++)
            {
                _top = _top.Next;
            }
        }

        public int Peek()
        {
            return _top.Value;
        }

        public Node PeekNode()
        {
            return _top;
        }

        public StackAsList Clone()
        {
            return new StackAsList {_top = _top};
        }

        public Tuple<int, Node> Key()
        {
            return new Tuple<int, Node>(_top.Value, _top.Next);
        }
    }
    
    public class ParseTreeNode
    {
        private static int _idCount = 1;
        
        public int Identifier;
        public string Symbol;
        public string Token;
        public int LineNumber;
        public int ColumnNumber;
        public List<ParseTreeNode> Children;
        
        // fields used in TreeOptimizer
        public ParseTreeNode Parent; 
        public bool IsSymbolic; 
        
        public ParseTreeNode(Token token, bool isRoot = false)
        {
            Symbol = token.Type;
            Token = token.Lexeme;
            LineNumber = token.Line;
            ColumnNumber = token.Column;
            
            Children = new List<ParseTreeNode>();
            
            if (isRoot)
            {
                Identifier = 0;
            }
            else
            {
                Identifier = _idCount;
                _idCount++;
            }
        }
        
        public ParseTreeNode(string symbol, bool isRoot = false)
        {
            Symbol = symbol;
            Token = "";
            LineNumber = -1;
            ColumnNumber = -1;
            
            Children = new List<ParseTreeNode>();
            
            if (isRoot)
            {
                Identifier = 0;
            }
            else
            {
                Identifier = _idCount;
                _idCount++;
            }
        }

        public ParseTreeNode(ParseTreeNode src)
        {
            Identifier = _idCount;
            _idCount++;
            Symbol = src.Symbol;
            Token = src.Token;
            Children = new List<ParseTreeNode>();
            foreach (var child in src.Children)
            {
                Children.Add(new ParseTreeNode(child));
            }
        }

        public override int GetHashCode() // used in TreeOptimizer: don't care about Token
        {
            int hash = Symbol.GetHashCode();
            foreach (var child in Children)
            {
                hash ^= child.GetHashCode();
            }
            return hash;
        }

        public override bool Equals(object obj) // used in TreeOptimizer: don't care about Token
        {
            if (obj == null) { return false; }
            var o = obj as ParseTreeNode;
            if (o == null) { return false; }

            if (IsSymbolic || o.IsSymbolic)
            {
                return true;
            }
            
            return Symbol == o.Symbol && Children.SequenceEqual(o.Children);
        }
        
        public static bool operator ==(ParseTreeNode o1, ParseTreeNode o2)
        {
            return Equals(o1, o2);
        }
        
        public static bool operator !=(ParseTreeNode o1, ParseTreeNode o2)
        {
            return !(o1 == o2);
        }
    }

    public class DfaState
    {
        private static int _idCount = 1;
        
        public readonly int Identifier;
        public readonly HashSet<Lr0Item> ItemSet;
        public readonly Dictionary<string, DfaState> Transitions;
        
        public DfaState(HashSet<Lr0Item> itemSet, bool isRoot = false)
        {
            ItemSet = itemSet;
            Transitions = new Dictionary<string, DfaState>();
            
            if (isRoot)
            {
                Identifier = 0;
            }
            else
            {
                Identifier = _idCount;
                _idCount++;
            }
        }
    }

    public class Lr0Item
    {
        public readonly string Lhs;
        public readonly List<string> Rhs;
        public readonly int Dpos; //index of thing after dist. pos.
        
        public Lr0Item(string lhs, List<string> rhs, int dpos)
        {
            Lhs = lhs;
            Rhs = rhs;
            Dpos = dpos;
        }

        public string ItemAtDpos()
        {
            return Rhs[Dpos];
        }
        
        public bool DposAtEnd()
        {
            return Dpos == Rhs.Count;
        }
        
        public override int GetHashCode()
        {
            return Lhs.GetHashCode() ^ Rhs.GetHashCode() ^ Dpos.GetHashCode();
        }
        
        public override bool Equals(object oo)
        {
            if(oo == null) { return false; }
            var o = oo as Lr0Item;
            if(o == null) { return false; }
            return Lhs == o.Lhs && Dpos == o.Dpos && Rhs.SequenceEqual(o.Rhs);
        }
        
        public static bool operator ==(Lr0Item o1, Lr0Item o2)
        {
            return Equals(o1, o2);
        }
        
        public static bool operator !=(Lr0Item o1, Lr0Item o2)
        {
            return !(o1 == o2);
        }
    }

    internal class Lr0AsKeyComparator : IEqualityComparer<HashSet<Lr0Item>> 
    {
        public Lr0AsKeyComparator()
        {
            
        }
        
        public bool Equals(HashSet<Lr0Item> a, HashSet<Lr0Item> b)
        {
            return a.SetEquals(b);
        }
        
        public int GetHashCode(HashSet<Lr0Item> x)
        {
            int h = 0;
            foreach(var i in x)
            {
                h ^= i.GetHashCode();
            }
            return h;
        }
    }
    
    public class LrRule
    {
        private readonly string _i1;
        private readonly int _i2;
        private readonly string _i3;

        private LrRule(string i1, int i2, string i3)
        {
            _i1 = i1;
            _i2 = i2;
            _i3 = i3;
        }

        public static LrRule TransitionRule(int i2)
        {
            return new LrRule("T", i2, "");
        }
        
        public static LrRule ShiftRule(int i2)
        {
            return new LrRule("S", i2, "");
        }
        
        public static LrRule ReduceRule(int i2, string i3)
        {
            return new LrRule("R", i2, i3);
        }

        public string GetRuleType()
        {
            return _i1;
        }
        
        public int GetAsTransitionOrShift()
        {
            return _i2;
        }
        
        public Tuple<int, string> GetAsReduce()
        {
            return new Tuple<int, string>(_i2, _i3);
        }

        public int GetReduceNumPop()
        {
            return _i2;
        }
        
        public string GetReduceTo()
        {
            return _i3;
        }
        
        public override int GetHashCode()
        {
            return _i1.GetHashCode() ^ _i2.GetHashCode() ^ _i3.GetHashCode();
        }
        
        public override bool Equals(object oo)
        {
            if(oo == null) { return false; }
            var o = oo as LrRule;
            if(o == null) { return false; }
            return _i1 == o._i1 && _i2 == o._i2 && _i3 == o._i3;
        }
        
        public static bool operator ==(LrRule o1, LrRule o2)
        {
            return Equals(o1, o2);
        }
        
        public static bool operator !=(LrRule o1, LrRule o2)
        {
            return !(o1 == o2);
        }

        public override string ToString()
        {
            if (_i1 == "R")
            {
                return "(R, " + _i2 + ", " + _i3 + ")";
            }
            
            return "(" + _i1 + ", " + _i2 + ")";
        }
        
        public string ToFriendlyTableString()
        {
            if (_i1 == "R")
            {
                return "R, " + _i2 + ", " + _i3;
            }
            
            return _i1 + ", " + _i2;
        }
    }

    public class Parser
    {
        public readonly NonTerminal StartSymbol;
        public readonly Dictionary<string, Terminal> Tokens;
        public readonly Dictionary<string, NonTerminal> Productions;
        public readonly HashSet<Terminal> Terminals;
        public readonly HashSet<NonTerminal> NonTerminals;

        private readonly HashSet<NonTerminal> _nullable;
        private readonly Dictionary<string, HashSet<string>> _first;
        private readonly Dictionary<NonTerminal, HashSet<string>> _follow;

        private readonly List<DfaState> _states;
        
        private readonly List<Dictionary<string, List<LrRule>>> _glrTable;

        private DfaState _startState;

        public Parser(NonTerminal startSymbol, List<Dictionary<string, List<LrRule>>> glrTable, HashSet<Terminal> tokens, HashSet<NonTerminal> productions)
        {
            StartSymbol = startSymbol;
            
            Tokens = new Dictionary<string, Terminal>();
            Productions = new Dictionary<string, NonTerminal>();
            foreach (var token in tokens)
            {
                Tokens.Add(token.Lhs, token);
            }
            foreach (var nt in productions)
            {
                Productions.Add(nt.Lhs, nt);
            }

            Terminals = tokens;
            NonTerminals = productions;
            
            _glrTable = glrTable;
            
            Utility.SaveGlrParseTableToHtml("GlrTable.html", _glrTable, Terminals, NonTerminals);
        }
        
        public Parser(NonTerminal startSymbol, Dictionary<string, Terminal> tokens, Dictionary<string, NonTerminal> productions)
        {
            StartSymbol = startSymbol;
            Tokens = tokens;
            Productions = productions;
            Terminals = new HashSet<Terminal>(Tokens.Values);
            NonTerminals = new HashSet<NonTerminal>(Productions.Values);

            _states = new List<DfaState>();
            
            _glrTable = new List<Dictionary<string, List<LrRule>>>();
            
            _nullable = new HashSet<NonTerminal>();
            _first = new Dictionary<string, HashSet<string>>();
            _follow = new Dictionary<NonTerminal, HashSet<string>>();
            
            CalculateNullable();
            CalculateFirst();
            CalculateFollow();

            BuildTable();
        }

        private bool IsNonTerminal(string s)
        {
            return Productions.ContainsKey(s);
        }
        
        private bool IsTerminal(string s)
        {
            return Tokens.ContainsKey(s);
        }

        private bool IsNullable(string s)
        {
            return IsNonTerminal(s) && _nullable.Contains(Productions[s]);
        }

        private void CalculateNullable()
        {
            bool working = true;
            while (working)
            {
                working = false;
                foreach (NonTerminal n in NonTerminals)
                {
                    if (!_nullable.Contains(n))
                    {
                        foreach (Production p in n.Rhs)
                        {
                            bool allNullable = true;
                            foreach (string symbol in p.Rhs)
                            {
                                if (IsNonTerminal(symbol))
                                {
                                    if (_nullable.Contains(Productions[symbol])) { continue; }
                                    
                                    allNullable = false;
                                    break;
                                }

                                allNullable = false;
                                break;
                            }

                            if (allNullable)
                            {
                                _nullable.Add(n);
                                working = true;
                            }
                        }
                    }
                }
            }
        }
        
        private void CalculateFirst()
        {
            foreach (Terminal terminal in Terminals)
            {
                _first.Add(terminal.Lhs, new HashSet<string> { terminal.Lhs });
            }

            foreach (NonTerminal nonTerminal in NonTerminals)
            {
                _first.Add(nonTerminal.Lhs, new HashSet<string>());
            }
            
            bool working = true;
            while (working)
            {
                working = false;
                foreach (NonTerminal n in NonTerminals)
                {
                    foreach (Production p in n.Rhs)
                    {
                        foreach (string x in p.Rhs)
                        {                            
                            int initialSize = _first[n.Lhs].Count;
                            _first[n.Lhs].UnionWith(_first[x]);
                            if (_first[n.Lhs].Count != initialSize)
                            {
                                working = true;
                            }

                            if (!IsNullable(x))
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }
        
        private void CalculateFollow()
        {
            foreach (NonTerminal nonTerminal in NonTerminals)
            {
                _follow.Add(nonTerminal, new HashSet<string>());
            }
            _follow[StartSymbol].Add("$");

            bool working = true;
            while (working)
            {
                working = false;
                foreach (NonTerminal n in NonTerminals)
                {
                    foreach (Production p in n.Rhs)
                    {
                        for (int i = 0; i < p.Rhs.Count; i++)
                        {
                            string xStr = p.Rhs[i];
                            if (IsNonTerminal(xStr))
                            {
                                NonTerminal x = Productions[xStr];
                                
                                bool brokeOut = false;
                                for (int j = i + 1; j < p.Rhs.Count; j++)
                                {
                                    string y = p.Rhs[j];
                                    int initialSize = _follow[x].Count;
                                    _follow[x].UnionWith(_first[y]);
                                    if (_follow[x].Count != initialSize)
                                    {
                                        working = true;
                                    }
                                    if (!IsNullable(y))
                                    {
                                        brokeOut = true;
                                        break;
                                    }
                                }

                                if (!brokeOut)
                                {
                                    int initialSize = _follow[x].Count;
                                    _follow[x].UnionWith(_follow[n]);
                                    if (_follow[x].Count != initialSize)
                                    {
                                        working = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        private HashSet<Lr0Item> ComputeClosure(HashSet<Lr0Item> s)
        {
            var s2 = new HashSet<Lr0Item>(s);
            var toConsider = new List<Lr0Item>(s);
            int i = 0;
            while (i < toConsider.Count)
            {
                var item = toConsider[i++];
                if (item.DposAtEnd())
                {
                    continue;
                }
                
                string sym = item.ItemAtDpos();
                if (!IsNonTerminal(sym))
                {
                    continue;
                }
                
                foreach (var p in Productions[sym].Rhs)
                {
                    var item2 = new Lr0Item(sym, p.Rhs, 0);
                    if (s2.Contains(item2))
                    {
                        continue;
                    }
                    
                    s2.Add(item2);
                    toConsider.Add(item2);
                }
            }
            
            return s2;
        }

        private static Dictionary<string, HashSet<Lr0Item>> ComputeTransitions(DfaState q)
        {
            var transitions = new Dictionary<string, HashSet<Lr0Item>>();
            foreach (var i in q.ItemSet)
            {
                if (i.DposAtEnd())
                {
                    continue;
                }
                
                string sym = i.ItemAtDpos();
                if (!transitions.ContainsKey(sym))
                {
                    transitions.Add(sym, new HashSet<Lr0Item>());
                }
                transitions[sym].Add(new Lr0Item(i.Lhs, i.Rhs, i.Dpos + 1));
            }
            return transitions;
        }

        private void AddStates(DfaState q, Dictionary<string, HashSet<Lr0Item>> transitions, IDictionary<HashSet<Lr0Item>, DfaState> seen, Stack<DfaState> todo)
        {
            foreach ((string sym, HashSet<Lr0Item> value) in transitions)
            {
                var i2 = ComputeClosure(value);
                
                if (!seen.ContainsKey(i2))
                {
                    var q2 = new DfaState(i2);
                    seen.Add(i2, q2);
                    todo.Push(q2);
                    _states.Add(q2);
                }
                
                q.Transitions.Add(sym, seen[i2]);
            }
        }
        
        private void BuildLr0Dfa()
        {
            _startState = new DfaState(ComputeClosure(new HashSet<Lr0Item>() { new Lr0Item("S'", new List<string>() { StartSymbol.Lhs }, 0)}), true);
            var todo = new Stack<DfaState>();
            todo.Push(_startState);
            var seen = new Dictionary<HashSet<Lr0Item>, DfaState>(new Lr0AsKeyComparator());
            seen.Add(_startState.ItemSet, _startState);
            _states.Add(_startState);
            while (todo.Count > 0)
            {
                var q = todo.Pop();
                var transitions = ComputeTransitions(q);
                AddStates(q, transitions, seen, todo);
            }

            Utility.SaveDfaToDot("lr0dfa.dot", _startState);
        }
        
        private void BuildTable()
        {
            BuildLr0Dfa();
            
            var sp = new NonTerminal("S'", new List<List<string>> { new List<string> { "S" } });
            _follow.Add(sp, new HashSet<string> { "$" });
            Productions.Add(sp.Lhs, sp);
            int conflicts = 0;
            foreach (DfaState s in _states)
            {
                var row = new Dictionary<string, List<LrRule>>();
                
                foreach ((string sym, DfaState v) in s.Transitions)
                {
                    if (!row.ContainsKey(sym))
                    {
                        row.Add(sym, new List<LrRule>());
                    }
                    var rule = IsTerminal(sym) ? LrRule.ShiftRule(v.Identifier) : LrRule.TransitionRule(v.Identifier);
                    if (!row[sym].Contains(rule))
                    {
                        if (row[sym].Count != 0)
                        {
                            conflicts++;
                        }
                        row[sym].Add(rule);
                    }
                }
                foreach (var item in s.ItemSet)
                {
                    if (!item.DposAtEnd())
                    {
                        continue;
                    }

                    foreach (string lhs in _follow[Productions[item.Lhs]])
                    {
                        if (!row.ContainsKey(lhs))
                        {
                            row.Add(lhs, new List<LrRule>());
                        }
                        
                        var rule = LrRule.ReduceRule(item.Rhs.Count, item.Lhs);
                        if (!row[lhs].Contains(rule))
                        {
                            if (row[lhs].Count != 0)
                            {
                                conflicts++;
                            }
                            row[lhs].Add(rule);
                        }
                    }
                    // row.Add("$", LrRule.ReduceRule(item.Rhs.Count, item.Lhs));
                }
                _glrTable.Add(row);
            }
            if(Program.ProgramOptions.VerbosityDebug)
            {
                Console.WriteLine("<DEBUG> # of Table Cells with >1 entry: " + conflicts);
            }
            Utility.SaveGlrParseTableToHtml("GlrTable.html", _glrTable, Terminals, NonTerminals);
            Utility.SaveGlrParseTableToFile("apris.glr", _glrTable, Terminals, NonTerminals);
        }

        private StackAsList ParseGlrAlgo(IReadOnlyList<Token> tokenStream)
        {
            var currentStacks = new List<StackAsList> { new StackAsList() };
            currentStacks[0].Push(0, null);
            int tokenIndex = 0;

            for (;;)
            {
                var nextToken = tokenStream[tokenIndex];
                
                // apply reductions
                var nextStacks = new List<StackAsList>();
                var active = new HashSet<Tuple<int, StackAsList.Node>>();
                int stackIndex = 0;
                while (stackIndex < currentStacks.Count)
                {
                    var stk = currentStacks[stackIndex];
                    int stateNumber = stk.Peek();
                    
                    if(stateNumber < _glrTable.Count && _glrTable[stateNumber].ContainsKey(nextToken.Type))
                    {
                        foreach (var op in _glrTable[stateNumber][nextToken.Type])
                        {
                            // process table entry
                            if (op.GetRuleType() == "R")
                            {
                                if (op.GetAsReduce().Item2 == "S'")
                                {
                                    // parse success
                                    return stk;
                                }

                                // perform reduction
                                (int numPop, string tSym) = op.GetAsReduce();
                                var stk2 = stk.Clone();
                                for (int i = 0; i < numPop; i++)
                                {
                                    stk2.Pop();
                                }

                                var newState = _glrTable[stk2.Peek()][tSym][0]; // odd
                                stk2.Push(newState.GetAsTransitionOrShift(),
                                    new Tuple<StackAsList.Node, LrRule>(stk.PeekNode(), LrRule.ReduceRule(numPop, tSym)));
                                if (active.Contains(stk2.Key()))
                                {
                                    continue;
                                }

                                active.Add(stk2.Key());
                                nextStacks.Add(stk2);
                                currentStacks.Add(stk2);
                            }
                            else
                            {
                                if (active.Contains(stk.Key()))
                                {
                                    continue;
                                }
                                
                                nextStacks.Add(stk);
                                active.Add(stk.Key());
                            }
                        }
                    }
                    stackIndex += 1;
                }
                
                // apply shifts
                currentStacks = nextStacks;
                nextStacks = new List<StackAsList>();
                foreach (var stk in currentStacks)
                {
                    int stateNumber = stk.Peek();
                    LrRule rule = null;
                    if (stateNumber < _glrTable.Count && _glrTable[stateNumber].ContainsKey(nextToken.Type))
                    {
                        foreach (var lrRule in _glrTable[stateNumber][nextToken.Type])
                        {
                            if (lrRule.GetRuleType() == "R")
                            {
                                continue;
                            }

                            if (rule != null)
                            {
                                throw new Exception("Hmm, what do?");
                            }

                            rule = lrRule;
                        }
                    }

                    if (rule == null)
                    {
                        continue;
                    }
                    
                    stk.Push(rule.GetAsTransitionOrShift(),
                        new Tuple<StackAsList.Node, LrRule>(stk.PeekNode(), LrRule.ShiftRule(tokenIndex)));
                    nextStacks.Add(stk);
                }
                currentStacks = nextStacks;
                
                if (currentStacks.Count == 0)
                {
                    // parse fail
                    return null;
                }

                tokenIndex += 1;
            }
        }
        
        public ParseTreeNode Parse(List<Token> tokenStream)
        {
            tokenStream.Add(new Token("$", "$", -1, -1));

            var successfulStack = ParseGlrAlgo(tokenStream);
            if (successfulStack == null)
            {
                return null;
            }

            var actions = new List<Tuple<StackAsList.Node, LrRule>>();
            var n = successfulStack.PeekNode();
            while (n.How != null)
            {
                actions.Add(new Tuple<StackAsList.Node, LrRule>(n, n.How.Item2));
                n = n.How.Item1;
            }
            actions.Reverse();

            var treeNodeStack = new List<ParseTreeNode>();
            foreach ((StackAsList.Node _, LrRule how) in actions)
            {
                if (how.GetRuleType() != "R")
                {
                    int ti = how.GetAsTransitionOrShift();
                    var token = tokenStream[ti];
                    treeNodeStack.Add(new ParseTreeNode(token));
                }
                else
                {
                    (int numPop, string tSym) = how.GetAsReduce();
                    var node = new ParseTreeNode(tSym);
                    for (int i = 0; i < numPop; i++)
                    {
                        var c = treeNodeStack[treeNodeStack.Count - 1];
                        treeNodeStack.RemoveAt(treeNodeStack.Count - 1);
                        node.Children.Insert(0, c);
                    }
                    treeNodeStack.Add(node);
                }
            }

            treeNodeStack[0].Identifier = 0;
            return treeNodeStack[0];
        }
    }
}
