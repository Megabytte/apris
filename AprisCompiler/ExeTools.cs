using System;
using System.Diagnostics;
using System.IO;

namespace AprisCompiler
{
    static class ExeTools
    {
        public static bool UseLinuxSubsystem = false;
        public static bool ExeTestMode = false;

        public enum Os
        {
            Win, Mac, Linux, Unknown
        }

        public static void Run(string input, string cmd, params string[] argsA)
        {
            string args;
            if (argsA.Length == 0)
            {
                args = "";
            }
            else
            {
                string[] tmp = new string[argsA.Length];
                for (int i = 0; i < argsA.Length; ++i)
                {
                    tmp[i] = '"' + argsA[i] + '"';
                }
                args = string.Join(" ", argsA);
            }
            
            var si = new ProcessStartInfo
            {
                WorkingDirectory = Directory.GetCurrentDirectory(),
                FileName = UseLinuxSubsystem ? "bash" : cmd,
                Arguments = UseLinuxSubsystem ? "-c \"" + cmd + " " + args + "\"" : args,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false
            };
            var proc = new Process {StartInfo = si};
            proc.OutputDataReceived += (s, a) =>
            {
                if(a?.Data != null)
                {
                    if (ExeTestMode)
                    {
                        Console.Write("Program Output: ");
                        Console.WriteLine(a.Data);
                    }
                    else
                    {
                        Console.WriteLine(a.Data);
                    }
                }
            };
            proc.ErrorDataReceived += (s, a) =>
            {
                if(a?.Data != null)
                {
                    if (ExeTestMode)
                    {
                        Console.Write("Program Error Output: ");
                    }
                    Console.WriteLine(a.Data);
                }
            };
            proc.Start();
            if (input.Length > 0)
            {
                proc.StandardInput.Write(input);
                proc.StandardInput.Write("\n");
            }
            proc.StandardInput.Flush();
            proc.StandardInput.Close();
            proc.BeginOutputReadLine();
            proc.BeginErrorReadLine();
            proc.WaitForExit();

            if (ExeTestMode)
            {
                Console.WriteLine("Process Returned: " + proc.ExitCode);
            }
            else
            {
                if (proc.ExitCode != 0)
                {
                    throw new Exception("Process failed: " + cmd + " " + args);
                }
            }
        }

        /** Input: Assembly code.
         * Returns: Object file*/
        public static void Assemble(string asmfile, string objfile)
        {
            switch (OperatingSystem)
            {
                case Os.Linux:
                    Run("", "nasm", "-f", "elf64", "-o", objfile, asmfile);
                    break;
                case Os.Win:
                    // , "-l xyz.lst"
                    Run("", "nasm", "-f", "win64", "-o", objfile, asmfile);
                    break;
                case Os.Mac:
                    Run("", "nasm", "--prefix", "_", "-f", "macho64", "-o", objfile, asmfile);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static void Link(string objfile, string exefile)
        {
            File.Delete(exefile);

            switch (OperatingSystem)
            {
                case Os.Linux:
                    Run("", "gcc", "-no-pie -m64", objfile, "-o", exefile);
                    break;
                case Os.Win:
                    // "c:\program files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat" 
                    // link xyz.obj /OUT:xyz.exe /LARGEADDRESSAWARE:NO /SUBSYSTEM:CONSOLE /nologo msvcrt.lib legacy_stdio_definitions.lib
                    var inp = String.Format(@"""c:\program files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat"" && cd ""{0}"" && link ""{1}"" ""/OUT:{2}"" /LARGEADDRESSAWARE:NO /SUBSYSTEM:CONSOLE /nologo msvcrt.lib legacy_stdio_definitions.lib",
                        Directory.GetCurrentDirectory(), objfile, exefile);
                    Run(inp, "cmd");
                    break;
                case Os.Mac:
                    Run("", "ld", "-o", exefile, objfile, "-macosx_version_min", "10.13", "-lSystem");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            if (!File.Exists(exefile))
            {
                throw new Exception("Link failed");
            }
        }

        static Os opsys = Os.Unknown;
        public static Os OperatingSystem
        {
            get
            {
                if (opsys != Os.Unknown)
                {
                    return opsys;
                }

                switch (Environment.OSVersion.Platform)
                {
                    case PlatformID.MacOSX:
                        opsys = Os.Mac;
                        break;
                    case PlatformID.Unix:
                        //https://stackoverflow.com/questions/38790802/determine-operating-system-in-net-core
                        try
                        {
                            string s = File.ReadAllText("/proc/sys/kernel/ostype");
                            opsys = s.IndexOf("inux") != -1 ? Os.Linux : Os.Mac;
                        }
                        catch (Exception)
                        {
                            opsys = Os.Mac;
                        }
                        break;
                    default:
                        opsys = Os.Win;
                        break;
                }

                if(UseLinuxSubsystem)
                {
                    opsys = Os.Linux;
                }

                return opsys;
            }
        }
    }
}